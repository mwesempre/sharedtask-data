README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Romanian, edition 1.2.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

**Raw corpora (UD-parsed) for the 1.2 edition**
------
The raw corpus for the PARSEME 1.2 shared task contains 447,464 sentences from online Romanian newspapers. 
All texts were shuffled so that the original articles cannot be recovered.
The corpus is tokenized into 12,822,588 tokens and parsed with UDPIPE (http://ufal.mff.cuni.cz/udpipe). 
The model used is `romanian-rrt-ud-2.5-191206` (available at https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3131).
The corpus is split into 100 files of almost the same size.
The corpus processing was performed by Elena Irimia.

**Data annotated for verbal MWEs**

The verbal MWE annotated data for the 1.2 edition corresponds to the 1.1 edition,
with some morpho-syntactic annotation corrections performed mainly with respect to verbal MWEs.

Corpora
-------
All annotated data comes from the "Agenda" newspaper. Some of them are part of the Romanian Universal Dependencies treebank (RoRefTrees, RRT). For this edition of the shared task, the corpus only underwent annotation improvement.


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe).
* UPOS (column 4): Available. Automatically annotated (UDPipe).
* XPOS (column 5): Available. Automatically annotated (UDPipe).
* FEATS (column 6): Available. Automatically annotated (UDPipe).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated (UDPipe).
* DEPS (column 8): Available. Automatically annotated (UDPipe).
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, IRV.

The UDPipe annotation relied on the model `romanian-ud-2.0-170801.udpipe`.


Tokenization
------------
The training and testing files are annotated with the TTL tool (developed at ICIA, by Radu Ion, 2007).


Authors
-------
All VMWEs annotations were performed by Verginica Barbu Mititelu, Mihaela Ionescu Cristescu, Mihaela Onofrei, and Monica-Mihaela Rizea.


License
-------
The data are distributed under the terms of the CC BY v4 License.


Contact
-------
vergi@racai.ro 
