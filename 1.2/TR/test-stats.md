Statistics TR
=============
### test.cupt
* Sentences: 3304
* Tokens: 48791
* Total VMWEs: 1151
  * `LVC.full`: 546
  * `MVC`: 1
  * `VID`: 604
* Unseen w.r.t. train: 307
* Ratio of unseen w.r.t. train: 0.26672458731537796
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.26064291920069504
