README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Hindi, edition 1.2

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Corpora
-------
All annotated data come from the Test section of the [Hindi Treebank](http://ltrc.iiit.ac.in/treebank_H2014), which is newswire text.  
All raw corpora are a portion of the [HindiMonocorp](https://lindat.mff.cuni.cz/repository/xmlui/handle/11858/00-097C-0000-0023-6260-A) which has been scraped from the web.


Annotated corpus
--------------------
The annotated data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. The size and type of annotations remain the same as the 1.1. shared task.  
Here is detailed information about some columns:

*  ID (column 1)  
*  FORM  (column 2)  
*  LEMMA (column 3)  
*  UPOS (column 4) : Automatically annotated (UDPipe)  
*  XPOS  (column 5): Automatically annotated (UDPipe)
*  FEATS (column 6): Automatically annotated (UDPipe)
*  HEAD (column 7) : Automatically annotated (UDPipe)
*  DEPREL (column 8): Automatically annotated(UDPipe)
*  DEPS (column 9): Automatically annotated (UDPipe)
*  MISC (column 10)
*  PARSEME:MWE (column 11): Manually annotated
 The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: LVC.cause, LVC.full, MVC, VID.

The UDPipe annotation relied on the model `model hindi-hdtb-ud-2.5-191206.udpipe` for the *annotated* versions of the corpus

Raw corpus
--------------------
The raw corpus is provided in CONLL format and has been parsed automatically using the model `model hindi-hdtb-ud-2.5-191206.udpipe`.  

Tokenization
------------
* Hyphens: Split as a single token.


Licence
-------
The annotated dataset is licensed under **Creative Commons Non-Commercial Share-Alike 4.0** licence [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
The raw dataset is licensed under **Creative Commons Non-Commercial Share-Alike 4.0** licence [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Authors
-------
Archna Bhatia.  
Ashwini Vaidya  

Contact
-------
abhatia@ihmc.us  
avaidya@hss.iitd.ac.in  
