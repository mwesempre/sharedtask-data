Statistics ZH
=============
### train.cupt
* Sentences: 35326
* Tokens: 575590
* Total VMWEs: 8113
  * `LVC.cause`: 148
  * `LVC.full`: 927
  * `MVC`: 3206
  * `VID`: 676
  * `VPC.semi`: 3156
