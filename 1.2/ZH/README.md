Introduction
------
This is a Chinese corpus containing sentences annotated with verbal multiword expressions (VMWEs), following the PARSEME guideline, edition 1.2. The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Source
-------
The annotated data comes from [Universal Dependencies](https://universaldependencies.org/) and [CoNLL 2017 Shared Task](http://universaldependencies.org/conll17/), where _Crawl-000_ consists of text from web pages and _Wiki-000_ consists of text from wikipedia Traditional Chinese. The detailed information is in the following:

|Corups|Source|Sentences|Tokens|
|------|------|---------|------|
|GSD-dev|UD Chinese GSD v2.5|500|12663|
|GSD-test|UD Chinese GSD v2.5|500|12012|
|GSD-train|UD Chinese GSD v2.5|3997|98616|
|HK-test|UD Chinese v2.1|1004|9874|
|PUD-test|UD Chinese v2.1|1000|21415|
|Crawl-000|CoNLL 2017 Shared Task|30274|445531|
|Wiki-000|CoNLL 2017 Shared Task|2654|49465|

Tokenization
-------------
The UD Chinese part is the manually annotated from UD Chinese guideline. The CoNLL 2017 Shared Task part was tokenized using the UDPipe tool v2.5, except that some of the segmentation errors have been corrected if the segmentation errors have affected the annotation of the right scope of a VMWE. For example, _處理 得乾 淨_ will be corrected as _處理 得 乾淨_, and then _處理_ and _乾淨_ will be annotated as VPC.semi.


Annotations
--------------------
To be indenpendent to the segmentation issues in Chinese that sometimes a string can be either one or two words depending on the principle, we annotate both single-token VMWEs (but certainly multiple morphemes) and multi-token VMWEs. 


|VMWE Type|Single Token|Double Token|Multi-Token|
|---------|-----------|-----------|----------|
|LVC.cause|0|161|6|
|LVC.full|0|1006|48|
|MVC|1495|2116|12|
|VID|670|27|60
|VPC.semi|2252|1304|8|
 

Known issues
-------------
Information about lemmas is missing for some sentences.  The missing lemmas are represented by the underscore `_` in the `LEMMA` column.


Licence
-------
The full dataset is licensed under Creative Commons Non-Commercial Share-Alike 4.0 licence CC-BY-NC-SA 4.0.


Authors
-------
Hongzhi Xu, Menghan Jiang


Contributors
-------
Jia Chen, Xiaomin Ge, Fangyuan Hu, Sha Hu, Minli Li, Siyuan Liu, Zhenzhen Qin, Ruilong Sun, Chengwen Wang, Huangyang Xiao, Peiyi Yan, Tsy Yih, Ke Yu, Songping Yu, Si Zeng, Yongchen Zhang, Yun Zhao


Raw corpus
-------
The raw corpus contains 4 files:
 * `raw-001.conllu`
    * origin: `conll2017-crawl-000`
    * Sentence count: 348935
    * Token count: 5150812
 * `raw-002.conllu`
    * origin: `conll2017-crawl-001`
    * Sentence count: 1175265
    * Token count: 18450549
 * `raw-003.conllu`
    * origin: `conll2017-crawl-002`
    * Sentence count: 1144700
    * Token count: 18588146
 * `raw-004.conllu`
    * origin: `conll2017-wiki-000`
    * Sentence count: 1438279
    * Token count: 25046404



Contact
-------
Hongzhi Xu: hxu@shisu.edu.cn

Menghan Jiang: menghanengl.jiang@polyu.edu.hk

