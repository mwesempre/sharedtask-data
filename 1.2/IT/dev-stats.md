Statistics IT
=============
### dev.cupt
* Sentences: 1202
* Tokens: 32652
* Total VMWEs: 324
  * `IAV`: 44
  * `IRV`: 81
  * `LS.ICV`: 5
  * `LVC.cause`: 18
  * `LVC.full`: 52
  * `MVC`: 4
  * `VID`: 109
  * `VPC.full`: 11
