Statistics IT
=============
### train.cupt
* Sentences: 10641
* Tokens: 292065
* Total VMWEs: 2854
  * `IAV`: 343
  * `IRV`: 783
  * `LS.ICV`: 20
  * `LVC.cause`: 112
  * `LVC.full`: 502
  * `MVC`: 19
  * `VID`: 999
  * `VPC.full`: 74
  * `VPC.semi`: 2
