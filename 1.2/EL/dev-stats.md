Statistics EL
=============
### dev.cupt
* Sentences: 909
* Tokens: 23911
* Total VMWEs: 315
  * `LVC.cause`: 5
  * `LVC.full`: 203
  * `MVC`: 5
  * `VID`: 98
  * `VPC.full`: 4
