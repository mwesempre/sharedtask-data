README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Greek, edition 1.2.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Corpora
-------
The annotated data come from the EL wikipedia, online news portals and online versions of newspapers and magazines 
([kathimerini](http://www.kathimerini.gr), [tovima](http://www.tovima.gr), [tanea](http://www.tanea.gr), [avgi](http://avgi.gr), [protothema](http://protothema.gr), [in.gr](http://www.in.gr), [iefimerida](https://www.iefimerida.gr), [efsyn](https://www.efsyn.gr), [protagon](http://www.protagon.gr), [capital](http://www/capital.gr), [newsit](https://www.newsit.gr), [espresso](https://www.espresso.gr)), and a subpart of the Greek section of the Universal Dependencies treebank. Version 1.1 of the corpus was processed again with the latest UD model for EL. Manual annotations of VMWEs from version 1.1. were made compatible with the latest [Annotation Guidelines](https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.2) version 1.2 via multiple semi-automatic concistency checks. In this respect, the EL annotated corpus has been rendered as homogenous as possible. Version 1.2 of the corpus was enriched with new data from the above-mentioned sources.

The raw data were drawn from the Greek section of the [CoNLL 2017 shared task corpus](http://hdl.handle.net/11234/1-1989), as well as the newswire and wikipedia subcorpora [Leipzig Corpus collection](https://wortschatz.uni-leipzig.de/en/download/) (Goldhahn et al., 2012). They were re-parsed with  UDPipe using the `greek-gdt-ud-2.5-191206` model.

Provided annotations
--------------------

The data are in the .cupt format. Here is detailed information about some columns:

 * LEMMA (column 3): Available. Automatically annotated.
 * UPOS (column 4): Available. Automatically annotated (UDPipe).
 * XPOS (column 5): Available. Automatically annotated (UDPipe).
 * FEATS (column 6): Available. Automatically annotated (UDPipe).
 * HEAD (column 7): Available. Automatically annotated (UDPipe).
 * DEPREL (column 8): Available. Automatically annotated (UDPipe).
 * DEPS (column 9): Available. Automatically annotated (UDPipe).
 * MISC (column 10): No-space information available. Automatically annotated.
 * PARSEME:MWE (column 11): Manually annotated. The following VMWE categories are annotated: VID, LVC.full, LVC.cause, VPC.full.

The UDPipe annotation relied on the model `greek-gdt-ud-2.5-191206`.

Tokenization
------------
* Contractions: Most contractions are kept as a single unit (not-split).  Only the forms _στου_ (_στης_, _στον_, _στη_, _στην_, _στο_, _στων_, _στους_, _στις_, _στα_) are split as two tokens _σ_ and _του_ (_της_, _τον_, _τη_, _την_, _το_, _των_, _τους_, _τις_, _τα_).


Licence
-------
The PARSEME-EL data are distributed under the terms of the CC BY-NC-SA 4.0 license. The [CoNLL 2017 shared task](http://hdl.handle.net/11234/1-1989) part of the raw corpus is distributed under CC BY-NC-SA 4.0 license, whereas, the [Leipzig Corpus collection](https://wortschatz.uni-leipzig.de/en/download/) provided for download is licensed under CC BY.



Authors
-------
Voula Giouli, Vassiliki Foufi, Aggeliki Fotopoulou, and Stella Markantonatou annotated new data in version 1.2. Stella Papadelli and Sevasti Louizou contributed to the annotations in versions 1.0 and 1.1 respectively.

References
----------
D. Goldhahn, T. Eckart & U. Quasthoff. 2012. Building Large Monolingual Dictionaries at the Leipzig Corpora Collection: From 100 to 200 Languages. In: Proceedings of the 8th International Language Ressources and Evaluation (LREC'12).


Contact
voula@athenarc.gr
