Statistics EL
=============
### test.cupt
* Sentences: 2805
* Tokens: 75442
* Total VMWEs: 974
  * `LVC.cause`: 19
  * `LVC.full`: 612
  * `MVC`: 3
  * `VID`: 323
  * `VPC.full`: 17
* Unseen w.r.t. train: 308
* Ratio of unseen w.r.t. train: 0.3162217659137577
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.3080082135523614
