Statistics PT
=============
### dev.cupt
* Sentences: 1976
* Tokens: 43676
* Total VMWEs: 397
  * `IRV`: 73
  * `LVC.cause`: 6
  * `LVC.full`: 236
  * `MVC`: 2
  * `VID`: 80
