Statistics SV
=============
### dev.cupt
* Sentences: 596
* Tokens: 8889
* Total VMWEs: 270
  * `IRV`: 24
  * `LVC.full`: 42
  * `VID`: 40
  * `VPC.full`: 108
  * `VPC.semi`: 56
