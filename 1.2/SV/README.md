README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Swedish, edition 1.2. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Swedish data are annotated from scratch for this version of the shared task.

The raw corpus is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task)

Source corpora
-------
All the annotated data come from the Swedish-Talbanken corpus, as distributed by the [Universal dependencies project](https://universaldependencies.org/). The annotation covers 4304 sentences (all 504 sentences from the sv_talbanken_ud_dev original file, and the first 3800 sentences from sv_talbanken_ud_train). The domain of the sentences are news texts and non-fiction (including high-school essays).


Format
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets are used:
* column 3 (lemma): available,
* column 4 (UPOS): [UD POS-tags](http://universaldependencies.org/u/pos) version 2.0, 
* column 5 (XPOS): [SUC tagset](https://cl.lingfil.uu.se/~nivre/swedish_treebank/pos.html) The language-specific tags (including features) follow the guidelines of the Stockholm-Umeå Corpus.
* column 6 (FEATS): [UD features](http://universaldependencies.org/u/feat/index.html) version 2.0
* column 8 (DEPREL): [UD dependency relations](http://universaldependencies.org/u/dep) version 2.0 
* column 11 (PARSEME:MWE): [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) version 1.2

All UD annotations are originally manually annotated, than converted to UD, and manually checked.
The reason for using UD 2.0 is for consistency with the autoamtic annotations in the raw corpus.

Companion raw corpus
--------------------
The manually annotated corpus, described above, is accompanied by a large "raw" corpus (meant for automatic discovery of VMWEs), in which VMWEs are not annotated and morphosyntax is automatically tagged. Its characteristics are the following:

* size (uncompressed): 175.5 GB
* sentences: 163,633,807
* tokens: 2,474,800,065
* tokens/sentence: 15.1
* format: CoNLL-U (version 2.0)
* source: [CoNLL 2017 shared task raw corpus](http://hdl.handle.net/11234/1-1989) for Swedish (see the [paper](https://www.aclweb.org/anthology/K17-3001.pdf#page=3))
* genre: Various web pages (from CommonCrawl)
* 2017 UDPipe baseline system, trained on Swedish-Talbanken version 2.0 (model: swedish-ud-2.0-conll17-170315.udpipe)
* Same UD version (2.0), but annotated corpus has manual annotations whereas raw corpus is annotated automatically.

Authors
----------
The VMWEs annotations (column 11) were performed by  Elsa Erenmalm, Gustav Finnveden, Bernadeta Griciūtė, Ellinor Lindqvist, Eva Pettersson and Sara Stymne (Language leader).
The additional preparation of the annotated and raw corpora was performed by Sara Stymne.

License
----------
The VMWEs annotations (column 11) are distributed under the terms of the [CC-BY v4](https://creativecommons.org/licenses/by/4.0/) license.
Swedish-Talbanken, with the annotations in all columns except 11, is realeased under the terms of the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
The annotations in the raw corpus (lemmas, POS-tags, features, dependency relations) are distributed under the terms of the [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/) license. The texts are available under the [Common Crawl terms of use](https://commoncrawl.org/terms-of-use/)

Contact
----------

*  sara.stymne@lingfil.uu.se
