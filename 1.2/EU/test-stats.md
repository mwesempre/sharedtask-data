Statistics EU
=============
### test.cupt
* Sentences: 5300
* Tokens: 75431
* Total VMWEs: 2020
  * `LVC.cause`: 106
  * `LVC.full`: 1508
  * `VID`: 406
* Unseen w.r.t. train: 353
* Ratio of unseen w.r.t. train: 0.17475247524752474
* Unseen w.r.t. train+dev: 300
* Ratio of unseen w.r.t. train+dev: 0.1485148514851485
