PARSEME Shared Task 1.1 - system results
========================================================================

This folder contains the raw results of the 
[PARSEME Shared Task 1.1](http://multiword.sourceforge.net/sharedtask2018/)
including system scores, system predictions and their descriptions.
System scores were also formatted and published on the
[Shared Task results page](http://multiword.sourceforge.net/sharedtaskresults2018/)

More detailed descriptions of some submitted systems were published as 
system description papers in the proceedings of the 
[LAW-MWE-CxG workshop 2018](http://multiword.sourceforge.net/lawmwecxg2018/)
which took place in Santa Fe (USA) on August 24-25, 2018.

### Folder structure

Each folder corresponds to a system. The `.closed` or `.open` extensions
indicate to which track the system was submitted. Each system folder 
contains a minimal `README.txt`, and one folder per language covered by
the system, with two files in it:
 * A `test.system.cupt` file, containing the system predictions for the
    given language in [cupt format](http://multiword.sourceforge.net/cupt-format/)
 * A `results.txt` file containing the scores for the system in that
    language, according to the shared task evaluation metrics

Each system folder also contains a `ave-results.txt` file with the 
macro-averaged results across all languages. A minimal `README.txt` 
provides a brief system description, and the author names.

