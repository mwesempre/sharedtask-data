## Global evaluation
* MWE-based: P=572/650=0.8800 R=572/776=0.7371 F=0.8022
* Tok-based: P=755/837=0.9020 R=755/1030=0.7330 F=0.8088

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=28/776=4% pred=30/650=5%
* LVC.cause: MWE-based: P=20/30=0.6667 R=20/28=0.7143 F=0.6897
* LVC.cause: Tok-based: P=43/51=0.8431 R=43/55=0.7818 F=0.8113
* LVC.full: MWE-proportion: gold=166/776=21% pred=131/650=20%
* LVC.full: MWE-based: P=85/131=0.6489 R=85/166=0.5120 F=0.5724
* LVC.full: Tok-based: P=176/232=0.7586 R=176/314=0.5605 F=0.6447
* VID: MWE-proportion: gold=10/776=1% pred=8/650=1%
* VID: MWE-based: P=5/8=0.6250 R=5/10=0.5000 F=0.5556
* VID: Tok-based: P=12/13=0.9231 R=12/17=0.7059 F=0.8000
* VPC.full: MWE-proportion: gold=486/776=63% pred=405/650=62%
* VPC.full: MWE-based: P=386/405=0.9531 R=386/486=0.7942 F=0.8664
* VPC.full: Tok-based: P=442/464=0.9526 R=442/556=0.7950 F=0.8667
* VPC.semi: MWE-proportion: gold=86/776=11% pred=77/650=12%
* VPC.semi: MWE-based: P=73/77=0.9481 R=73/86=0.8488 F=0.8957
* VPC.semi: Tok-based: P=74/78=0.9487 R=74/88=0.8409 F=0.8916

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=711/776=92% pred=602/650=93%
* Continuous: MWE-based: P=540/602=0.8970 R=540/711=0.7595 F=0.8225
* Discontinuous: MWE-proportion: gold=65/776=8% pred=48/650=7%
* Discontinuous: MWE-based: P=32/48=0.6667 R=32/65=0.4923 F=0.5664

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=253/776=33% pred=185/650=28%
* Multi-token: MWE-based: P=156/185=0.8432 R=156/253=0.6166 F=0.7123
* Single-token: MWE-proportion: gold=523/776=67% pred=465/650=72%
* Single-token: MWE-based: P=416/465=0.8946 R=416/523=0.7954 F=0.8421

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=704/776=91% pred=562/650=86%
* Seen-in-train: MWE-based: P=560/562=0.9964 R=560/704=0.7955 F=0.8847
* Unseen-in-train: MWE-proportion: gold=72/776=9% pred=88/650=14%
* Unseen-in-train: MWE-based: P=12/88=0.1364 R=12/72=0.1667 F=0.1500

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=149/704=21% pred=91/562=16%
* Variant-of-train: MWE-based: P=89/91=0.9780 R=89/149=0.5973 F=0.7417
* Identical-to-train: MWE-proportion: gold=555/704=79% pred=471/562=84%
* Identical-to-train: MWE-based: P=471/471=1.0000 R=471/555=0.8486 F=0.9181

