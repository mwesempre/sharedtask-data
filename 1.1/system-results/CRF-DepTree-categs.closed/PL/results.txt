## Global evaluation
* MWE-based: P=229/859=0.2666 R=229/515=0.4447 F=0.3333
* Tok-based: P=642/1690=0.3799 R=642/1108=0.5794 F=0.4589

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=29/515=6% pred=129/859=15%
* IAV: MWE-based: P=3/129=0.0233 R=3/29=0.1034 F=0.0380
* IAV: Tok-based: P=23/143=0.1608 R=23/64=0.3594 F=0.2222
* IRV: MWE-proportion: gold=249/515=48% pred=413/859=48%
* IRV: MWE-based: P=175/413=0.4237 R=175/249=0.7028 F=0.5287
* IRV: Tok-based: P=456/863=0.5284 R=456/499=0.9138 F=0.6696
* LVC.cause: MWE-proportion: gold=15/515=3% pred=41/859=5%
* LVC.cause: MWE-based: P=0/41=0.0000 R=0/15=0.0000 F=0.0000
* LVC.cause: Tok-based: P=1/57=0.0175 R=1/33=0.0303 F=0.0222
* LVC.full: MWE-proportion: gold=149/515=29% pred=162/859=19%
* LVC.full: MWE-based: P=39/162=0.2407 R=39/149=0.2617 F=0.2508
* LVC.full: Tok-based: P=109/366=0.2978 R=109/305=0.3574 F=0.3249
* VID: MWE-proportion: gold=73/515=14% pred=128/859=15%
* VID: MWE-based: P=9/128=0.0703 R=9/73=0.1233 F=0.0896
* VID: Tok-based: P=35/279=0.1254 R=35/207=0.1691 F=0.1440

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=361/515=70% pred=616/859=72%
* Continuous: MWE-based: P=180/616=0.2922 R=180/361=0.4986 F=0.3685
* Discontinuous: MWE-proportion: gold=154/515=30% pred=243/859=28%
* Discontinuous: MWE-based: P=49/243=0.2016 R=49/154=0.3182 F=0.2469

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=515/515=100% pred=556/859=65%
* Multi-token: MWE-based: P=229/556=0.4119 R=229/515=0.4447 F=0.4276

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=371/515=72% pred=207/859=24%
* Seen-in-train: MWE-based: P=197/207=0.9517 R=197/371=0.5310 F=0.6817
* Unseen-in-train: MWE-proportion: gold=144/515=28% pred=652/859=76%
* Unseen-in-train: MWE-based: P=32/652=0.0491 R=32/144=0.2222 F=0.0804

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=223/371=60% pred=115/207=56%
* Variant-of-train: MWE-based: P=108/115=0.9391 R=108/223=0.4843 F=0.6391
* Identical-to-train: MWE-proportion: gold=148/371=40% pred=92/207=44%
* Identical-to-train: MWE-based: P=89/92=0.9674 R=89/148=0.6014 F=0.7417

