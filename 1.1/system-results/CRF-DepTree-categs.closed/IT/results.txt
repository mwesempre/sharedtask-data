## Global evaluation
* MWE-based: P=128/286=0.4476 R=128/496=0.2581 F=0.3274
* Tok-based: P=385/655=0.5878 R=385/1292=0.2980 F=0.3955

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=26/286=9%
* IAV: MWE-based: P=10/26=0.3846 R=10/41=0.2439 F=0.2985
* IAV: Tok-based: P=31/56=0.5536 R=31/101=0.3069 F=0.3949
* IRV: MWE-proportion: gold=96/496=19% pred=76/286=27%
* IRV: MWE-based: P=35/76=0.4605 R=35/96=0.3646 F=0.4070
* IRV: Tok-based: P=77/136=0.5662 R=77/192=0.4010 F=0.4695
* LS.ICV: MWE-proportion: gold=8/496=2% pred=2/286=1%
* LS.ICV: MWE-based: P=1/2=0.5000 R=1/8=0.1250 F=0.2000
* LS.ICV: Tok-based: P=2/5=0.4000 R=2/17=0.1176 F=0.1818
* LVC.cause: MWE-proportion: gold=25/496=5% pred=11/286=4%
* LVC.cause: MWE-based: P=6/11=0.5455 R=6/25=0.2400 F=0.3333
* LVC.cause: Tok-based: P=13/22=0.5909 R=13/53=0.2453 F=0.3467
* LVC.full: MWE-proportion: gold=104/496=21% pred=61/286=21%
* LVC.full: MWE-based: P=31/61=0.5082 R=31/104=0.2981 F=0.3758
* LVC.full: Tok-based: P=73/120=0.6083 R=73/231=0.3160 F=0.4160
* MVC: MWE-proportion: gold=5/496=1% pred=1/286=0%
* MVC: MWE-based: P=1/1=1.0000 R=1/5=0.2000 F=0.3333
* MVC: Tok-based: P=2/2=1.0000 R=2/11=0.1818 F=0.3077
* VID: MWE-proportion: gold=201/496=41% pred=105/286=37%
* VID: MWE-based: P=30/105=0.2857 R=30/201=0.1493 F=0.1961
* VID: Tok-based: P=131/306=0.4281 R=131/652=0.2009 F=0.2735
* VPC.full: MWE-proportion: gold=23/496=5% pred=11/286=4%
* VPC.full: MWE-based: P=6/11=0.5455 R=6/23=0.2609 F=0.3529
* VPC.full: Tok-based: P=16/24=0.6667 R=16/50=0.3200 F=0.4324

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=220/286=77%
* Continuous: MWE-based: P=94/220=0.4273 R=94/331=0.2840 F=0.3412
* Discontinuous: MWE-proportion: gold=165/496=33% pred=66/286=23%
* Discontinuous: MWE-based: P=34/66=0.5152 R=34/165=0.2061 F=0.2944

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=237/286=83%
* Multi-token: MWE-based: P=128/237=0.5401 R=128/496=0.2581 F=0.3492

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=125/286=44%
* Seen-in-train: MWE-based: P=111/125=0.8880 R=111/297=0.3737 F=0.5261
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=161/286=56%
* Unseen-in-train: MWE-based: P=17/161=0.1056 R=17/199=0.0854 F=0.0944

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=63/125=50%
* Variant-of-train: MWE-based: P=57/63=0.9048 R=57/184=0.3098 F=0.4615
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=62/125=50%
* Identical-to-train: MWE-based: P=54/62=0.8710 R=54/113=0.4779 F=0.6171

