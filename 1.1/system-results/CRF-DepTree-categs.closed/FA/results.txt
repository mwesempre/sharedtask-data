## Global evaluation
* MWE-based: P=289/372=0.7769 R=289/501=0.5768 F=0.6621
* Tok-based: P=666/757=0.8798 R=666/1113=0.5984 F=0.7123

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=0/501=0% pred=1/372=0%
* IRV: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* IRV: Tok-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=501/501=100% pred=369/372=99%
* LVC.full: MWE-based: P=288/369=0.7805 R=288/501=0.5749 F=0.6621
* LVC.full: Tok-based: P=662/751=0.8815 R=662/1113=0.5948 F=0.7103
* VID: MWE-proportion: gold=0/501=0% pred=2/372=1%
* VID: MWE-based: P=0/2=0.0000 R=0/0=0.0000 F=0.0000
* VID: Tok-based: P=0/5=0.0000 R=0/0=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=398/501=79% pred=339/372=91%
* Continuous: MWE-based: P=271/339=0.7994 R=271/398=0.6809 F=0.7354
* Discontinuous: MWE-proportion: gold=103/501=21% pred=33/372=9%
* Discontinuous: MWE-based: P=18/33=0.5455 R=18/103=0.1748 F=0.2647

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=346/372=93%
* Multi-token: MWE-based: P=289/346=0.8353 R=289/501=0.5768 F=0.6824

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=313/501=62% pred=213/372=57%
* Seen-in-train: MWE-based: P=206/213=0.9671 R=206/313=0.6581 F=0.7833
* Unseen-in-train: MWE-proportion: gold=188/501=38% pred=159/372=43%
* Unseen-in-train: MWE-based: P=83/159=0.5220 R=83/188=0.4415 F=0.4784

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=167/313=53% pred=95/213=45%
* Variant-of-train: MWE-based: P=92/95=0.9684 R=92/167=0.5509 F=0.7023
* Identical-to-train: MWE-proportion: gold=146/313=47% pred=118/213=55%
* Identical-to-train: MWE-based: P=114/118=0.9661 R=114/146=0.7808 F=0.8636

