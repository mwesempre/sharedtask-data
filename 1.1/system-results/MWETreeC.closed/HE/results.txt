## Global evaluation
* MWE-based: P=0/314=0.0000 R=0/502=0.0000 F=0.0000
* Tok-based: P=115/314=0.3662 R=115/1155=0.0996 F=0.1566

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=49/502=10% pred=41/314=13%
* LVC.cause: MWE-based: P=0/41=0.0000 R=0/49=0.0000 F=0.0000
* LVC.cause: Tok-based: P=13/41=0.3171 R=13/101=0.1287 F=0.1831
* LVC.full: MWE-proportion: gold=211/502=42% pred=143/314=46%
* LVC.full: MWE-based: P=0/143=0.0000 R=0/211=0.0000 F=0.0000
* LVC.full: Tok-based: P=48/143=0.3357 R=48/435=0.1103 F=0.1661
* VID: MWE-proportion: gold=182/502=36% pred=126/314=40%
* VID: MWE-based: P=0/126=0.0000 R=0/182=0.0000 F=0.0000
* VID: Tok-based: P=39/126=0.3095 R=39/467=0.0835 F=0.1315
* VPC.full: MWE-proportion: gold=60/502=12% pred=4/314=1%
* VPC.full: MWE-based: P=0/4=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/4=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=314/314=100%
* Continuous: MWE-based: P=0/314=0.0000 R=0/384=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=118/502=24% pred=0/314=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/118=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=0/314=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/502=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=0/314=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/174=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=314/314=100%
* Unseen-in-train: MWE-based: P=0/314=0.0000 R=0/328=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/72=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/102=0.0000 F=0.0000

