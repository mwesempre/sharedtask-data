## Global evaluation
* MWE-based: P=0/118397=0.0000 R=0/500=0.0000 F=0.0000
* Tok-based: P=500/118397=0.0042 R=500/1123=0.4452 F=0.0084

## Per-category evaluation (partition of Global)
* <unlabeled>: MWE-proportion: gold=0/500=0% pred=118397/118397=100%
* <unlabeled>: MWE-based: P=0/118397=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/118397=0.0000 R=0/0=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=14/500=3% pred=0/118397=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/14=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=284/500=57% pred=0/118397=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/284=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/569=0.0000 F=0.0000
* VID: MWE-proportion: gold=202/500=40% pred=0/118397=0%
* VID: MWE-based: P=0/0=0.0000 R=0/202=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/526=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=302/500=60% pred=118397/118397=100%
* Continuous: MWE-based: P=0/118397=0.0000 R=0/302=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=198/500=40% pred=0/118397=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/198=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=0/118397=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/500=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=225/500=45% pred=0/118397=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/225=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=275/500=55% pred=118397/118397=100%
* Unseen-in-train: MWE-based: P=0/118397=0.0000 R=0/275=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=186/225=83% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/186=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=39/225=17% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/39=0.0000 F=0.0000

