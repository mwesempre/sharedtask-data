## Global evaluation
* MWE-based: P=0/691=0.0000 R=0/589=0.0000 F=0.0000
* Tok-based: P=398/691=0.5760 R=398/1268=0.3139 F=0.4063

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=363/589=62% pred=276/691=40%
* IRV: MWE-based: P=0/276=0.0000 R=0/363=0.0000 F=0.0000
* IRV: Tok-based: P=219/276=0.7935 R=219/726=0.3017 F=0.4371
* LVC.cause: MWE-proportion: gold=19/589=3% pred=48/691=7%
* LVC.cause: MWE-based: P=0/48=0.0000 R=0/19=0.0000 F=0.0000
* LVC.cause: Tok-based: P=17/48=0.3542 R=17/56=0.3036 F=0.3269
* LVC.full: MWE-proportion: gold=34/589=6% pred=46/691=7%
* LVC.full: MWE-based: P=0/46=0.0000 R=0/34=0.0000 F=0.0000
* LVC.full: Tok-based: P=20/46=0.4348 R=20/71=0.2817 F=0.3419
* VID: MWE-proportion: gold=173/589=29% pred=322/691=47%
* VID: MWE-based: P=0/322=0.0000 R=0/173=0.0000 F=0.0000
* VID: Tok-based: P=139/322=0.4317 R=139/415=0.3349 F=0.3772

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=393/589=67% pred=691/691=100%
* Continuous: MWE-based: P=0/691=0.0000 R=0/393=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=196/589=33% pred=0/691=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/196=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=589/589=100% pred=0/691=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/589=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=561/589=95% pred=91/691=13%
* Seen-in-train: MWE-based: P=0/91=0.0000 R=0/561=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=28/589=5% pred=600/691=87%
* Unseen-in-train: MWE-based: P=0/600=0.0000 R=0/28=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=65/561=12% pred=49/91=54%
* Variant-of-train: MWE-based: P=0/49=0.0000 R=0/65=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=496/561=88% pred=42/91=46%
* Identical-to-train: MWE-based: P=0/42=0.0000 R=0/496=0.0000 F=0.0000

