## Global evaluation
* MWE-based: P=0/1530=0.0000 R=0/500=0.0000 F=0.0000
* Tok-based: P=47/1530=0.0307 R=47/1006=0.0467 F=0.0371

## Per-category evaluation (partition of Global)
* <unlabeled>: MWE-proportion: gold=0/500=0% pred=1071/1530=70%
* <unlabeled>: MWE-based: P=0/1071=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/1071=0.0000 R=0/0=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=17/500=3% pred=6/1530=0%
* LVC.cause: MWE-based: P=0/6=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/6=0.0000 R=0/34=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=410/500=82% pred=391/1530=26%
* LVC.full: MWE-based: P=0/391=0.0000 R=0/410=0.0000 F=0.0000
* LVC.full: Tok-based: P=15/391=0.0384 R=15/821=0.0183 F=0.0248
* VID: MWE-proportion: gold=73/500=15% pred=62/1530=4%
* VID: MWE-based: P=0/62=0.0000 R=0/73=0.0000 F=0.0000
* VID: Tok-based: P=1/62=0.0161 R=1/151=0.0066 F=0.0094

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=407/500=81% pred=1530/1530=100%
* Continuous: MWE-based: P=0/1530=0.0000 R=0/407=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=93/500=19% pred=0/1530=0%
* Discontinuous: MWE-based: P=0/0=0.0000 R=0/93=0.0000 F=0.0000

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=0/1530=0%
* Multi-token: MWE-based: P=0/0=0.0000 R=0/500=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=422/500=84% pred=0/1530=0%
* Seen-in-train: MWE-based: P=0/0=0.0000 R=0/422=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=78/500=16% pred=1530/1530=100%
* Unseen-in-train: MWE-based: P=0/1530=0.0000 R=0/78=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=164/422=39% pred=0/0=0%
* Variant-of-train: MWE-based: P=0/0=0.0000 R=0/164=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=258/422=61% pred=0/0=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/258=0.0000 F=0.0000

