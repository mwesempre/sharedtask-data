MWETreeC
Anne-Laure MEALIER, Carlos Ramisch, Alexis Nasr and Julien Zoubian
(Submission #20)

Summary
The system that we present here is called MWETreeC: MWE Classification through Syntactic Trees. It is divided into three parts. In the first time, we represent each anotated sentence of the corpus "train.cupt" in the form of a syntactic tree. Each node of the tree represents a word in the sentence (with all its characteristics, from ID to MWEs). We describe in the form of syntactic subtrees, rules corresponding to the various MWEs. Then, We search for all possible matches between the subtrees and the syntaxic tree of each sentence. For each node of the tree (word), we store in a file, the application of the correspondences of the rules. When a word is not matched, we assign it the symbol '*'. This file is the input file of a Forest Random Classifier. In a second time, we train a Forest Random classifier to learn the correspondences between the application of rules and the expressions of MWEs. The classifier predictions from the corpus "test.blind.cupt", are stored, word by word, in a text file. These predictions are in the form of a list: "LVC.full, ID, IRV, etc.). In a third time, we generate the output file "test.system.cupt". To do this, we need to reformat our prediction data in ".cupt" format. For example, for a MWE such as VPC.full, the.cupt format requires that the verb to be noted "1:VPC.full", and that the adverb to be noted "1". In our system, the Classifier only learns the categories (i.e. "VPC.full, ID, etc). So, we use the syntactic trees again to find the links between the words corresponding to the MWEs to generate the output file. The first and the third part are in C ++ (they use the standard library). The second part is in Python. The classifier comes from the Scikit-learn library. We also use the Tokenizer function of the Keras library.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)
