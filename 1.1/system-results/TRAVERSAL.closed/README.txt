TRAVERSAL
Jakub Waszczuk
(Submission #15)

Summary
The system relies on the assumption that MWEs form connected syntactic components, i.e., that lexical elements of a single MWE occurrence should be adjacent in the dependency analysis of the underlying sentence. Based on this assumption, we represent the task of MWE recognition as a labeling task where one of two labels (MWE or not-MWE) must be predicted for each node in the dependency tree based on local contextual information: dependency labels, word forms, lemmas, POS tags, etc., as well as the MWE/not-MWE labels assigned to adjacent nodes. To this end, our system strives to determine the globally optimal labeling of the entire tree. This method can be seen as an extension of sequential conditional random fields to tree structures or, alternatively, as a graph-based dependency parser which labels the nodes but considers the dependency structure as fixed.

In our experiments, we used the Polish and French development (dev) datasets to determine a reasonable set of feature templates for our system. Afterwards, for each language where the development set was available, we trained the parsing models over the combined train+dev dataset. We trained one model per MWE type (and per language) so as to handle the phenomenon of overlapping MWEs of different types, often occurring in the provided annotated datasets. Finally, we left the problem of adjacent MWEs of the same type aside and assumed that, if adjacent nodes are individually marked as MWEs of the same type, then they must correspond to one MWE occurrence (with the exception of Farsi, for which we used a different heuristic to handle MWE segmentation). We note, however, that the proposed labeling method could be most likely extended to handle IOB-like encodings.

For selected languages we applied certain pre-processing procedures (notably case lifting, which consisted in reattaching case dependents so as to make MWEs of certain types, e.g. IAV, connected) -- in order to facilitate prediction. Arguably, one of the by-design disadvantages of the implemented method is that it is not able to deal with MWEs spanning disconnected fragments of dependency trees.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)
