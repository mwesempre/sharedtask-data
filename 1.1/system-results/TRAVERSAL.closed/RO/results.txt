## Global evaluation
* MWE-based: P=469/545=0.8606 R=469/589=0.7963 F=0.8272
* Tok-based: P=1043/1174=0.8884 R=1043/1268=0.8226 F=0.8542

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=363/589=62% pred=361/545=66%
* IRV: MWE-based: P=300/361=0.8310 R=300/363=0.8264 F=0.8287
* IRV: Tok-based: P=618/721=0.8571 R=618/726=0.8512 F=0.8542
* LVC.cause: MWE-proportion: gold=19/589=3% pred=20/545=4%
* LVC.cause: MWE-based: P=19/20=0.9500 R=19/19=1.0000 F=0.9744
* LVC.cause: Tok-based: P=56/57=0.9825 R=56/56=1.0000 F=0.9912
* LVC.full: MWE-proportion: gold=34/589=6% pred=26/545=5%
* LVC.full: MWE-based: P=22/26=0.8462 R=22/34=0.6471 F=0.7333
* LVC.full: Tok-based: P=48/54=0.8889 R=48/71=0.6761 F=0.7680
* VID: MWE-proportion: gold=173/589=29% pred=138/545=25%
* VID: MWE-based: P=128/138=0.9275 R=128/173=0.7399 F=0.8232
* VID: Tok-based: P=321/342=0.9386 R=321/415=0.7735 F=0.8481

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=393/589=67% pred=371/545=68%
* Continuous: MWE-based: P=320/371=0.8625 R=320/393=0.8142 F=0.8377
* Discontinuous: MWE-proportion: gold=196/589=33% pred=174/545=32%
* Discontinuous: MWE-based: P=149/174=0.8563 R=149/196=0.7602 F=0.8054

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=589/589=100% pred=532/545=98%
* Multi-token: MWE-based: P=469/532=0.8816 R=469/589=0.7963 F=0.8368

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=561/589=95% pred=502/545=92%
* Seen-in-train: MWE-based: P=467/502=0.9303 R=467/561=0.8324 F=0.8786
* Unseen-in-train: MWE-proportion: gold=28/589=5% pred=43/545=8%
* Unseen-in-train: MWE-based: P=2/43=0.0465 R=2/28=0.0714 F=0.0563

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=65/561=12% pred=49/502=10%
* Variant-of-train: MWE-based: P=30/49=0.6122 R=30/65=0.4615 F=0.5263
* Identical-to-train: MWE-proportion: gold=496/561=88% pred=453/502=90%
* Identical-to-train: MWE-based: P=437/453=0.9647 R=437/496=0.8810 F=0.9210

