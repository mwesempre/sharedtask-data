mumpitz
Rafael Ehren, Timm Lichte and Younes Samih
(Submission #18)

Summary
A bidirectional recurrent neural network composed of LSTM units that uses the lemmas and POS tags as features. It only conducts binary classification, i.e. it decides if a token is part of a verbal multiword expression (VMWE) or not. So after the classification a heuristic is used that leverages the HEAD information provided in the original data to differentiate the VMWEs in a sentence. The lemmas are not represented by random, but pretrained embeddings which hopefully raise the performance of the system. The word embeddings were created by us with the Word2vec implementation of the python package gensim and trained on the DECOW16 corpus created by Felix Bildhauer and Roland Schäfer (http://corporafromtheweb.org/).

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	OPEN
 	
Languages:  	
German (DE)
