TRAPACC_S
Regina Stodden and Behrang QasemiZadeh
(Submission #23)

Summary
Our system is a transition based parser with the stack of deep convolutional neural network and a SVM non-linear kernel as its classifier. We are using linguistic features that are available in the .cupt file, i.e., word form, lemma, morphological information, part of speech, universal part of speech, and dependency parses. The system is trained on both training and development datasets. We don’t use other resources than the provided corpora, i.e close track. A member of the team was involved in the creation of the shared task data sets.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Basque (EU)
Bulgarian (BG)
Croatian (HR)
German (DE)
Greek (EL)
English (EN)
Farsi (FA)
French (FR)
Hebrew (HE)
Hindi (HI)
Hungarian (HU)
Italian (IT)
Lithuanian (LT)
Polish (PL)
Brazilian Portuguese (PT)
Romanian (RO)
Slovene (SL)
Spanish (ES)
Turkish (TR)
