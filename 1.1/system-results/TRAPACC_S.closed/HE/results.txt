## Global evaluation
* MWE-based: P=50/88=0.5682 R=50/502=0.0996 F=0.1695
* Tok-based: P=127/184=0.6902 R=127/1155=0.1100 F=0.1897

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=49/502=10% pred=4/88=5%
* LVC.cause: MWE-based: P=1/4=0.2500 R=1/49=0.0204 F=0.0377
* LVC.cause: Tok-based: P=2/5=0.4000 R=2/101=0.0198 F=0.0377
* LVC.full: MWE-proportion: gold=211/502=42% pred=47/88=53%
* LVC.full: MWE-based: P=20/47=0.4255 R=20/211=0.0948 F=0.1550
* LVC.full: Tok-based: P=42/94=0.4468 R=42/435=0.0966 F=0.1588
* VID: MWE-proportion: gold=182/502=36% pred=36/88=41%
* VID: MWE-based: P=18/36=0.5000 R=18/182=0.0989 F=0.1651
* VID: Tok-based: P=51/83=0.6145 R=51/467=0.1092 F=0.1855
* VPC.full: MWE-proportion: gold=60/502=12% pred=1/88=1%
* VPC.full: MWE-based: P=0/1=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/2=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=77/88=88%
* Continuous: MWE-based: P=49/77=0.6364 R=49/384=0.1276 F=0.2126
* Discontinuous: MWE-proportion: gold=118/502=24% pred=11/88=12%
* Discontinuous: MWE-based: P=1/11=0.0909 R=1/118=0.0085 F=0.0155

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=85/88=97%
* Multi-token: MWE-based: P=50/85=0.5882 R=50/502=0.0996 F=0.1704

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=47/88=53%
* Seen-in-train: MWE-based: P=43/47=0.9149 R=43/174=0.2471 F=0.3891
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=41/88=47%
* Unseen-in-train: MWE-based: P=7/41=0.1707 R=7/328=0.0213 F=0.0379

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=16/47=34%
* Variant-of-train: MWE-based: P=12/16=0.7500 R=12/72=0.1667 F=0.2727
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=31/47=66%
* Identical-to-train: MWE-based: P=31/31=1.0000 R=31/102=0.3039 F=0.4662

