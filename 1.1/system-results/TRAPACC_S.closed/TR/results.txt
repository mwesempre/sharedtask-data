## Global evaluation
* MWE-based: P=3/268=0.0112 R=3/506=0.0059 F=0.0078
* Tok-based: P=21/350=0.0600 R=21/1045=0.0201 F=0.0301

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=0/506=0% pred=214/268=80%
* LVC.cause: MWE-based: P=0/214=0.0000 R=0/0=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/216=0.0000 R=0/0=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=272/506=54% pred=3/268=1%
* LVC.full: MWE-based: P=0/3=0.0000 R=0/272=0.0000 F=0.0000
* LVC.full: Tok-based: P=1/6=0.1667 R=1/548=0.0018 F=0.0036
* MVC: MWE-proportion: gold=1/506=0% pred=0/268=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VID: MWE-proportion: gold=233/506=46% pred=3/268=1%
* VID: MWE-based: P=2/3=0.6667 R=2/233=0.0086 F=0.0169
* VID: Tok-based: P=4/6=0.6667 R=4/495=0.0081 F=0.0160
* VPC.full: MWE-proportion: gold=0/506=0% pred=48/268=18%
* VPC.full: MWE-based: P=0/48=0.0000 R=0/0=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/122=0.0000 R=0/0=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=208/506=41% pred=261/268=97%
* Continuous: MWE-based: P=0/261=0.0000 R=0/208=0.0000 F=0.0000
* Discontinuous: MWE-proportion: gold=298/506=59% pred=7/268=3%
* Discontinuous: MWE-based: P=3/7=0.4286 R=3/298=0.0101 F=0.0197

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/506=99% pred=56/268=21%
* Multi-token: MWE-based: P=3/56=0.0536 R=3/502=0.0060 F=0.0108
* Single-token: MWE-proportion: gold=4/506=1% pred=212/268=79%
* Single-token: MWE-based: P=0/212=0.0000 R=0/4=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=126/506=25% pred=2/268=1%
* Seen-in-train: MWE-based: P=0/2=0.0000 R=0/126=0.0000 F=0.0000
* Unseen-in-train: MWE-proportion: gold=380/506=75% pred=266/268=99%
* Unseen-in-train: MWE-based: P=3/266=0.0113 R=3/380=0.0079 F=0.0093

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=75/126=60% pred=2/2=100%
* Variant-of-train: MWE-based: P=0/2=0.0000 R=0/75=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=51/126=40% pred=0/2=0%
* Identical-to-train: MWE-based: P=0/0=0.0000 R=0/51=0.0000 F=0.0000

