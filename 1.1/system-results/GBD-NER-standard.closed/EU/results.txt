## Global evaluation
* MWE-based: P=291/646=0.4505 R=291/500=0.5820 F=0.5079
* Tok-based: P=590/1292=0.4567 R=590/1006=0.5865 F=0.5135

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=17/500=3% pred=0/646=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/34=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=410/500=82% pred=646/646=100%
* LVC.full: MWE-based: P=259/646=0.4009 R=259/410=0.6317 F=0.4905
* LVC.full: Tok-based: P=523/1292=0.4048 R=523/821=0.6370 F=0.4950
* VID: MWE-proportion: gold=73/500=15% pred=0/646=0%
* VID: MWE-based: P=0/0=0.0000 R=0/73=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/151=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=407/500=81% pred=559/646=87%
* Continuous: MWE-based: P=276/559=0.4937 R=276/407=0.6781 F=0.5714
* Discontinuous: MWE-proportion: gold=93/500=19% pred=87/646=13%
* Discontinuous: MWE-based: P=15/87=0.1724 R=15/93=0.1613 F=0.1667

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=646/646=100%
* Multi-token: MWE-based: P=291/646=0.4505 R=291/500=0.5820 F=0.5079

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=422/500=84% pred=288/646=45%
* Seen-in-train: MWE-based: P=272/288=0.9444 R=272/422=0.6445 F=0.7662
* Unseen-in-train: MWE-proportion: gold=78/500=16% pred=358/646=55%
* Unseen-in-train: MWE-based: P=19/358=0.0531 R=19/78=0.2436 F=0.0872

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=164/422=39% pred=69/288=24%
* Variant-of-train: MWE-based: P=53/69=0.7681 R=53/164=0.3232 F=0.4549
* Identical-to-train: MWE-proportion: gold=258/422=61% pred=219/288=76%
* Identical-to-train: MWE-based: P=219/219=1.0000 R=219/258=0.8488 F=0.9182

