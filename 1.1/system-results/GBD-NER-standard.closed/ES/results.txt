## Global evaluation
* MWE-based: P=223/1885=0.1183 R=223/500=0.4460 F=0.1870
* Tok-based: P=615/3932=0.1564 R=615/1150=0.5348 F=0.2420

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=263/1885=14%
* IAV: MWE-based: P=12/263=0.0456 R=12/64=0.1875 F=0.0734
* IAV: Tok-based: P=38/539=0.0705 R=38/148=0.2568 F=0.1106
* IRV: MWE-proportion: gold=121/500=24% pred=617/1885=33%
* IRV: MWE-based: P=96/617=0.1556 R=96/121=0.7934 F=0.2602
* IRV: Tok-based: P=197/1234=0.1596 R=197/242=0.8140 F=0.2669
* LVC.cause: MWE-proportion: gold=28/500=6% pred=0/1885=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/63=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=85/500=17% pred=405/1885=21%
* LVC.full: MWE-based: P=14/405=0.0346 R=14/85=0.1647 F=0.0571
* LVC.full: Tok-based: P=32/810=0.0395 R=32/176=0.1818 F=0.0649
* MVC: MWE-proportion: gold=106/500=21% pred=512/1885=27%
* MVC: MWE-based: P=77/512=0.1504 R=77/106=0.7264 F=0.2492
* MVC: Tok-based: P=188/1123=0.1674 R=188/249=0.7550 F=0.2741
* VID: MWE-proportion: gold=95/500=19% pred=88/1885=5%
* VID: MWE-based: P=6/88=0.0682 R=6/95=0.0632 F=0.0656
* VID: Tok-based: P=26/226=0.1150 R=26/270=0.0963 F=0.1048
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/1885=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=1383/1885=73%
* Continuous: MWE-based: P=201/1383=0.1453 R=201/359=0.5599 F=0.2308
* Discontinuous: MWE-proportion: gold=141/500=28% pred=502/1885=27%
* Discontinuous: MWE-based: P=22/502=0.0438 R=22/141=0.1560 F=0.0684

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=1885/1885=100%
* Multi-token: MWE-based: P=223/1885=0.1183 R=223/500=0.4460 F=0.1870

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=378/1885=20%
* Seen-in-train: MWE-based: P=141/378=0.3730 R=141/279=0.5054 F=0.4292
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=1507/1885=80%
* Unseen-in-train: MWE-based: P=82/1507=0.0544 R=82/221=0.3710 F=0.0949

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=172/378=46%
* Variant-of-train: MWE-based: P=57/172=0.3314 R=57/145=0.3931 F=0.3596
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=206/378=54%
* Identical-to-train: MWE-based: P=84/206=0.4078 R=84/134=0.6269 F=0.4941

