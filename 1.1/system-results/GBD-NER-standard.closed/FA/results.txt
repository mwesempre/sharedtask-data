## Global evaluation
* MWE-based: P=371/475=0.7811 R=371/501=0.7405 F=0.7602
* Tok-based: P=842/996=0.8454 R=842/1113=0.7565 F=0.7985

## Per-category evaluation (partition of Global)
* LVC.full: MWE-proportion: gold=501/501=100% pred=475/475=100%
* LVC.full: MWE-based: P=371/475=0.7811 R=371/501=0.7405 F=0.7602
* LVC.full: Tok-based: P=842/996=0.8454 R=842/1113=0.7565 F=0.7985

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=398/501=79% pred=391/475=82%
* Continuous: MWE-based: P=326/391=0.8338 R=326/398=0.8191 F=0.8264
* Discontinuous: MWE-proportion: gold=103/501=21% pred=84/475=18%
* Discontinuous: MWE-based: P=45/84=0.5357 R=45/103=0.4369 F=0.4813

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=475/475=100%
* Multi-token: MWE-based: P=371/475=0.7811 R=371/501=0.7405 F=0.7602

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=313/501=62% pred=284/475=60%
* Seen-in-train: MWE-based: P=264/284=0.9296 R=264/313=0.8435 F=0.8844
* Unseen-in-train: MWE-proportion: gold=188/501=38% pred=191/475=40%
* Unseen-in-train: MWE-based: P=107/191=0.5602 R=107/188=0.5691 F=0.5646

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=167/313=53% pred=138/284=49%
* Variant-of-train: MWE-based: P=125/138=0.9058 R=125/167=0.7485 F=0.8197
* Identical-to-train: MWE-proportion: gold=146/313=47% pred=146/284=51%
* Identical-to-train: MWE-based: P=139/146=0.9521 R=139/146=0.9521 F=0.9521

