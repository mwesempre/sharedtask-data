## Global evaluation
* MWE-based: P=181/297=0.6094 R=181/498=0.3635 F=0.4553
* Tok-based: P=367/471=0.7792 R=367/975=0.3764 F=0.5076

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=40/498=8% pred=37/297=12%
* IRV: MWE-based: P=13/37=0.3514 R=13/40=0.3250 F=0.3377
* IRV: Tok-based: P=40/70=0.5714 R=40/96=0.4167 F=0.4819
* LVC.cause: MWE-proportion: gold=2/498=0% pred=0/297=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=42/498=8% pred=6/297=2%
* LVC.full: MWE-based: P=1/6=0.1667 R=1/42=0.0238 F=0.0417
* LVC.full: Tok-based: P=5/9=0.5556 R=5/97=0.0515 F=0.0943
* VID: MWE-proportion: gold=183/498=37% pred=88/297=30%
* VID: MWE-based: P=33/88=0.3750 R=33/183=0.1803 F=0.2435
* VID: Tok-based: P=115/166=0.6928 R=115/462=0.2489 F=0.3662
* VPC.full: MWE-proportion: gold=210/498=42% pred=165/297=56%
* VPC.full: MWE-based: P=119/165=0.7212 R=119/210=0.5667 F=0.6347
* VPC.full: Tok-based: P=174/225=0.7733 R=174/291=0.5979 F=0.6744
* VPC.semi: MWE-proportion: gold=23/498=5% pred=1/297=0%
* VPC.semi: MWE-based: P=0/1=0.0000 R=0/23=0.0000 F=0.0000
* VPC.semi: Tok-based: P=1/1=1.0000 R=1/31=0.0323 F=0.0625

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=267/498=54% pred=183/297=62%
* Continuous: MWE-based: P=113/183=0.6175 R=113/267=0.4232 F=0.5022
* Discontinuous: MWE-proportion: gold=231/498=46% pred=114/297=38%
* Discontinuous: MWE-based: P=68/114=0.5965 R=68/231=0.2944 F=0.3942

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=349/498=70% pred=174/297=59%
* Multi-token: MWE-based: P=104/174=0.5977 R=104/349=0.2980 F=0.3977
* Single-token: MWE-proportion: gold=149/498=30% pred=123/297=41%
* Single-token: MWE-based: P=77/123=0.6260 R=77/149=0.5168 F=0.5662

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=250/498=50% pred=151/297=51%
* Seen-in-train: MWE-based: P=124/151=0.8212 R=124/250=0.4960 F=0.6185
* Unseen-in-train: MWE-proportion: gold=248/498=50% pred=146/297=49%
* Unseen-in-train: MWE-based: P=57/146=0.3904 R=57/248=0.2298 F=0.2893

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=148/250=59% pred=74/151=49%
* Variant-of-train: MWE-based: P=62/74=0.8378 R=62/148=0.4189 F=0.5586
* Identical-to-train: MWE-proportion: gold=102/250=41% pred=77/151=51%
* Identical-to-train: MWE-based: P=62/77=0.8052 R=62/102=0.6078 F=0.6927

