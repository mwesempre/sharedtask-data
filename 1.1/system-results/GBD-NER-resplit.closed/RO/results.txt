## Global evaluation
* MWE-based: P=518/1842=0.2812 R=518/589=0.8795 F=0.4262
* Tok-based: P=1129/3914=0.2885 R=1129/1268=0.8904 F=0.4357

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=363/589=62% pred=827/1842=45%
* IRV: MWE-based: P=336/827=0.4063 R=336/363=0.9256 F=0.5647
* IRV: Tok-based: P=677/1654=0.4093 R=677/726=0.9325 F=0.5689
* LVC.cause: MWE-proportion: gold=19/589=3% pred=62/1842=3%
* LVC.cause: MWE-based: P=18/62=0.2903 R=18/19=0.9474 F=0.4444
* LVC.cause: Tok-based: P=55/177=0.3107 R=55/56=0.9821 F=0.4721
* LVC.full: MWE-proportion: gold=34/589=6% pred=178/1842=10%
* LVC.full: MWE-based: P=23/178=0.1292 R=23/34=0.6765 F=0.2170
* LVC.full: Tok-based: P=47/357=0.1317 R=47/71=0.6620 F=0.2196
* VID: MWE-proportion: gold=173/589=29% pred=775/1842=42%
* VID: MWE-based: P=130/775=0.1677 R=130/173=0.7514 F=0.2743
* VID: Tok-based: P=311/1726=0.1802 R=311/415=0.7494 F=0.2905

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=393/589=67% pred=1367/1842=74%
* Continuous: MWE-based: P=360/1367=0.2634 R=360/393=0.9160 F=0.4091
* Discontinuous: MWE-proportion: gold=196/589=33% pred=475/1842=26%
* Discontinuous: MWE-based: P=158/475=0.3326 R=158/196=0.8061 F=0.4709

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=589/589=100% pred=1842/1842=100%
* Multi-token: MWE-based: P=518/1842=0.2812 R=518/589=0.8795 F=0.4262

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=561/589=95% pred=653/1842=35%
* Seen-in-train: MWE-based: P=502/653=0.7688 R=502/561=0.8948 F=0.8270
* Unseen-in-train: MWE-proportion: gold=28/589=5% pred=1189/1842=65%
* Unseen-in-train: MWE-based: P=16/1189=0.0135 R=16/28=0.5714 F=0.0263

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=65/561=12% pred=137/653=21%
* Variant-of-train: MWE-based: P=38/137=0.2774 R=38/65=0.5846 F=0.3762
* Identical-to-train: MWE-proportion: gold=496/561=88% pred=516/653=79%
* Identical-to-train: MWE-based: P=464/516=0.8992 R=464/496=0.9355 F=0.9170

