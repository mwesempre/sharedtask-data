## Global evaluation
* MWE-based: P=221/998=0.2214 R=221/498=0.4438 F=0.2955
* Tok-based: P=599/2169=0.2762 R=599/1171=0.5115 F=0.3587

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=108/498=22% pred=182/998=18%
* IRV: MWE-based: P=62/182=0.3407 R=62/108=0.5741 F=0.4276
* IRV: Tok-based: P=126/372=0.3387 R=126/216=0.5833 F=0.4286
* LVC.cause: MWE-proportion: gold=14/498=3% pred=28/998=3%
* LVC.cause: MWE-based: P=1/28=0.0357 R=1/14=0.0714 F=0.0476
* LVC.cause: Tok-based: P=2/59=0.0339 R=2/28=0.0714 F=0.0460
* LVC.full: MWE-proportion: gold=160/498=32% pred=388/998=39%
* LVC.full: MWE-based: P=61/388=0.1572 R=61/160=0.3812 F=0.2226
* LVC.full: Tok-based: P=137/794=0.1725 R=137/333=0.4114 F=0.2431
* MVC: MWE-proportion: gold=4/498=1% pred=11/998=1%
* MVC: MWE-based: P=0/11=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/29=0.0000 R=0/8=0.0000 F=0.0000
* <unlabeled>: MWE-proportion: gold=0/498=0% pred=1/998=0%
* <unlabeled>: MWE-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/1=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=212/498=43% pred=388/998=39%
* VID: MWE-based: P=78/388=0.2010 R=78/212=0.3679 F=0.2600
* VID: Tok-based: P=256/914=0.2801 R=256/586=0.4369 F=0.3413

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=281/498=56% pred=450/998=45%
* Continuous: MWE-based: P=142/450=0.3156 R=142/281=0.5053 F=0.3885
* Discontinuous: MWE-proportion: gold=217/498=44% pred=548/998=55%
* Discontinuous: MWE-based: P=79/548=0.1442 R=79/217=0.3641 F=0.2065

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/498=100% pred=997/998=100%
* Multi-token: MWE-based: P=221/997=0.2217 R=221/498=0.4438 F=0.2957

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=251/498=50% pred=190/998=19%
* Seen-in-train: MWE-based: P=168/190=0.8842 R=168/251=0.6693 F=0.7619
* Unseen-in-train: MWE-proportion: gold=247/498=50% pred=808/998=81%
* Unseen-in-train: MWE-based: P=53/808=0.0656 R=53/247=0.2146 F=0.1005

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=126/251=50% pred=81/190=43%
* Variant-of-train: MWE-based: P=70/81=0.8642 R=70/126=0.5556 F=0.6763
* Identical-to-train: MWE-proportion: gold=125/251=50% pred=109/190=57%
* Identical-to-train: MWE-based: P=98/109=0.8991 R=98/125=0.7840 F=0.8376

