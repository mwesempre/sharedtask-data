## Global evaluation
* MWE-based: P=336/1137=0.2955 R=336/501=0.6707 F=0.4103
* Tok-based: P=891/2467=0.3612 R=891/1195=0.7456 F=0.4866

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=11/501=2% pred=0/1137=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=308/501=61% pred=1079/1137=95%
* LVC.full: MWE-based: P=234/1079=0.2169 R=234/308=0.7597 F=0.3374
* LVC.full: Tok-based: P=522/2271=0.2299 R=522/641=0.8144 F=0.3585
* MVC: MWE-proportion: gold=2/501=0% pred=3/1137=0%
* MVC: MWE-based: P=0/3=0.0000 R=0/2=0.0000 F=0.0000
* MVC: Tok-based: P=0/6=0.0000 R=0/6=0.0000 F=0.0000
* VID: MWE-proportion: gold=169/501=34% pred=55/1137=5%
* VID: MWE-based: P=15/55=0.2727 R=15/169=0.0888 F=0.1339
* VID: Tok-based: P=72/190=0.3789 R=72/500=0.1440 F=0.2087
* VPC.full: MWE-proportion: gold=11/501=2% pred=0/1137=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=275/501=55% pred=459/1137=40%
* Continuous: MWE-based: P=191/459=0.4161 R=191/275=0.6945 F=0.5204
* Discontinuous: MWE-proportion: gold=226/501=45% pred=678/1137=60%
* Discontinuous: MWE-based: P=145/678=0.2139 R=145/226=0.6416 F=0.3208

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=1137/1137=100%
* Multi-token: MWE-based: P=336/1137=0.2955 R=336/501=0.6707 F=0.4103

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=289/501=58% pred=269/1137=24%
* Seen-in-train: MWE-based: P=240/269=0.8922 R=240/289=0.8304 F=0.8602
* Unseen-in-train: MWE-proportion: gold=212/501=42% pred=868/1137=76%
* Unseen-in-train: MWE-based: P=96/868=0.1106 R=96/212=0.4528 F=0.1778

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=197/289=68% pred=181/269=67%
* Variant-of-train: MWE-based: P=157/181=0.8674 R=157/197=0.7970 F=0.8307
* Identical-to-train: MWE-proportion: gold=92/289=32% pred=88/269=33%
* Identical-to-train: MWE-based: P=83/88=0.9432 R=83/92=0.9022 F=0.9222

