## Global evaluation
* MWE-based: P=388/496=0.7823 R=388/501=0.7745 F=0.7783
* Tok-based: P=875/1040=0.8413 R=875/1113=0.7862 F=0.8128

## Per-category evaluation (partition of Global)
* LVC.full: MWE-proportion: gold=501/501=100% pred=496/496=100%
* LVC.full: MWE-based: P=388/496=0.7823 R=388/501=0.7745 F=0.7783
* LVC.full: Tok-based: P=875/1040=0.8413 R=875/1113=0.7862 F=0.8128

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=398/501=79% pred=401/496=81%
* Continuous: MWE-based: P=335/401=0.8354 R=335/398=0.8417 F=0.8385
* Discontinuous: MWE-proportion: gold=103/501=21% pred=95/496=19%
* Discontinuous: MWE-based: P=53/95=0.5579 R=53/103=0.5146 F=0.5354

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=501/501=100% pred=496/496=100%
* Multi-token: MWE-based: P=388/496=0.7823 R=388/501=0.7745 F=0.7783

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=313/501=62% pred=285/496=57%
* Seen-in-train: MWE-based: P=267/285=0.9368 R=267/313=0.8530 F=0.8930
* Unseen-in-train: MWE-proportion: gold=188/501=38% pred=211/496=43%
* Unseen-in-train: MWE-based: P=121/211=0.5735 R=121/188=0.6436 F=0.6065

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=167/313=53% pred=140/285=49%
* Variant-of-train: MWE-based: P=131/140=0.9357 R=131/167=0.7844 F=0.8534
* Identical-to-train: MWE-proportion: gold=146/313=47% pred=145/285=51%
* Identical-to-train: MWE-based: P=136/145=0.9379 R=136/146=0.9315 F=0.9347

