## Global evaluation
* MWE-based: P=242/2832=0.0855 R=242/502=0.4821 F=0.1452
* Tok-based: P=648/6001=0.1080 R=648/1155=0.5610 F=0.1811

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=49/502=10% pred=69/2832=2%
* LVC.cause: MWE-based: P=18/69=0.2609 R=18/49=0.3673 F=0.3051
* LVC.cause: Tok-based: P=40/140=0.2857 R=40/101=0.3960 F=0.3320
* LVC.full: MWE-proportion: gold=211/502=42% pred=1601/2832=57%
* LVC.full: MWE-based: P=109/1601=0.0681 R=109/211=0.5166 F=0.1203
* LVC.full: Tok-based: P=238/3269=0.0728 R=238/435=0.5471 F=0.1285
* VID: MWE-proportion: gold=182/502=36% pred=1155/2832=41%
* VID: MWE-based: P=57/1155=0.0494 R=57/182=0.3132 F=0.0853
* VID: Tok-based: P=167/2577=0.0648 R=167/467=0.3576 F=0.1097
* VPC.full: MWE-proportion: gold=60/502=12% pred=7/2832=0%
* VPC.full: MWE-based: P=0/7=0.0000 R=0/60=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/15=0.0000 R=0/152=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=384/502=76% pred=2126/2832=75%
* Continuous: MWE-based: P=203/2126=0.0955 R=203/384=0.5286 F=0.1618
* Discontinuous: MWE-proportion: gold=118/502=24% pred=706/2832=25%
* Discontinuous: MWE-based: P=39/706=0.0552 R=39/118=0.3305 F=0.0947

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/502=100% pred=2832/2832=100%
* Multi-token: MWE-based: P=242/2832=0.0855 R=242/502=0.4821 F=0.1452

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=174/502=35% pred=176/2832=6%
* Seen-in-train: MWE-based: P=139/176=0.7898 R=139/174=0.7989 F=0.7943
* Unseen-in-train: MWE-proportion: gold=328/502=65% pred=2656/2832=94%
* Unseen-in-train: MWE-based: P=103/2656=0.0388 R=103/328=0.3140 F=0.0690

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=72/174=41% pred=93/176=53%
* Variant-of-train: MWE-based: P=56/93=0.6022 R=56/72=0.7778 F=0.6788
* Identical-to-train: MWE-proportion: gold=102/174=59% pred=83/176=47%
* Identical-to-train: MWE-based: P=83/83=1.0000 R=83/102=0.8137 F=0.8973

