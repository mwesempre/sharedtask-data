## Global evaluation
* MWE-based: P=212/2219=0.0955 R=212/501=0.4232 F=0.1559
* Tok-based: P=491/4465=0.1100 R=491/1087=0.4517 F=0.1769

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=44/501=9% pred=13/2219=1%
* IAV: MWE-based: P=0/13=0.0000 R=0/44=0.0000 F=0.0000
* IAV: Tok-based: P=0/26=0.0000 R=0/98=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=36/501=7% pred=0/2219=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/36=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/72=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=166/501=33% pred=719/2219=32%
* LVC.full: MWE-based: P=33/719=0.0459 R=33/166=0.1988 F=0.0746
* LVC.full: Tok-based: P=71/1439=0.0493 R=71/340=0.2088 F=0.0798
* MVC: MWE-proportion: gold=4/501=1% pred=0/2219=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=79/501=16% pred=267/2219=12%
* VID: MWE-based: P=3/267=0.0112 R=3/79=0.0380 F=0.0173
* VID: Tok-based: P=10/560=0.0179 R=10/225=0.0444 F=0.0255
* VPC.full: MWE-proportion: gold=146/501=29% pred=1220/2219=55%
* VPC.full: MWE-based: P=115/1220=0.0943 R=115/146=0.7877 F=0.1684
* VPC.full: Tok-based: P=230/2440=0.0943 R=230/292=0.7877 F=0.1684
* VPC.semi: MWE-proportion: gold=26/501=5% pred=0/2219=0%
* VPC.semi: MWE-based: P=0/0=0.0000 R=0/26=0.0000 F=0.0000
* VPC.semi: Tok-based: P=0/0=0.0000 R=0/52=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=295/501=59% pred=1564/2219=70%
* Continuous: MWE-based: P=161/1564=0.1029 R=161/295=0.5458 F=0.1732
* Discontinuous: MWE-proportion: gold=206/501=41% pred=655/2219=30%
* Discontinuous: MWE-based: P=51/655=0.0779 R=51/206=0.2476 F=0.1185

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/501=99% pred=2219/2219=100%
* Multi-token: MWE-based: P=212/2219=0.0955 R=212/498=0.4257 F=0.1561
* Single-token: MWE-proportion: gold=3/501=1% pred=0/2219=0%
* Single-token: MWE-based: P=0/0=0.0000 R=0/3=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=143/501=29% pred=159/2219=7%
* Seen-in-train: MWE-based: P=87/159=0.5472 R=87/143=0.6084 F=0.5762
* Unseen-in-train: MWE-proportion: gold=358/501=71% pred=2060/2219=93%
* Unseen-in-train: MWE-based: P=125/2060=0.0607 R=125/358=0.3492 F=0.1034

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=76/143=53% pred=90/159=57%
* Variant-of-train: MWE-based: P=43/90=0.4778 R=43/76=0.5658 F=0.5181
* Identical-to-train: MWE-proportion: gold=67/143=47% pred=69/159=43%
* Identical-to-train: MWE-based: P=44/69=0.6377 R=44/67=0.6567 F=0.6471

