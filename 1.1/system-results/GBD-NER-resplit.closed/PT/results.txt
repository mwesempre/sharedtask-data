## Global evaluation
* MWE-based: P=350/2360=0.1483 R=350/553=0.6329 F=0.2403
* Tok-based: P=835/5092=0.1640 R=835/1247=0.6696 F=0.2634

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=91/553=16% pred=290/2360=12%
* IRV: MWE-based: P=74/290=0.2552 R=74/91=0.8132 F=0.3885
* IRV: Tok-based: P=148/580=0.2552 R=148/184=0.8043 F=0.3874
* LVC.cause: MWE-proportion: gold=7/553=1% pred=3/2360=0%
* LVC.cause: MWE-based: P=0/3=0.0000 R=0/7=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/7=0.0000 R=0/15=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=337/553=61% pred=1700/2360=72%
* LVC.full: MWE-based: P=210/1700=0.1235 R=210/337=0.6231 F=0.2062
* LVC.full: Tok-based: P=454/3480=0.1305 R=454/691=0.6570 F=0.2177
* VID: MWE-proportion: gold=118/553=21% pred=367/2360=16%
* VID: MWE-based: P=35/367=0.0954 R=35/118=0.2966 F=0.1443
* VID: Tok-based: P=112/1025=0.1093 R=112/357=0.3137 F=0.1621

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=313/553=57% pred=1059/2360=45%
* Continuous: MWE-based: P=211/1059=0.1992 R=211/313=0.6741 F=0.3076
* Discontinuous: MWE-proportion: gold=240/553=43% pred=1301/2360=55%
* Discontinuous: MWE-based: P=139/1301=0.1068 R=139/240=0.5792 F=0.1804

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=553/553=100% pred=2360/2360=100%
* Multi-token: MWE-based: P=350/2360=0.1483 R=350/553=0.6329 F=0.2403

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=397/553=72% pred=305/2360=13%
* Seen-in-train: MWE-based: P=278/305=0.9115 R=278/397=0.7003 F=0.7920
* Unseen-in-train: MWE-proportion: gold=156/553=28% pred=2055/2360=87%
* Unseen-in-train: MWE-based: P=72/2055=0.0350 R=72/156=0.4615 F=0.0651

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=233/397=59% pred=172/305=56%
* Variant-of-train: MWE-based: P=149/172=0.8663 R=149/233=0.6395 F=0.7358
* Identical-to-train: MWE-proportion: gold=164/397=41% pred=133/305=44%
* Identical-to-train: MWE-based: P=129/133=0.9699 R=129/164=0.7866 F=0.8687

