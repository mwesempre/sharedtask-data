## Global evaluation
* MWE-based: P=213/250=0.8520 R=213/500=0.4260 F=0.5680
* Tok-based: P=459/529=0.8677 R=459/1104=0.4158 F=0.5622

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=12/500=2% pred=2/250=1%
* LVC.cause: MWE-based: P=0/2=0.0000 R=0/12=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/4=0.0000 R=0/24=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=320/500=64% pred=137/250=55%
* LVC.full: MWE-based: P=119/137=0.8686 R=119/320=0.3719 F=0.5208
* LVC.full: Tok-based: P=263/303=0.8680 R=263/682=0.3856 F=0.5340
* MVC: MWE-proportion: gold=130/500=26% pred=106/250=42%
* MVC: MWE-based: P=87/106=0.8208 R=87/130=0.6692 F=0.7373
* MVC: Tok-based: P=176/212=0.8302 R=176/260=0.6769 F=0.7458
* VID: MWE-proportion: gold=38/500=8% pred=5/250=2%
* VID: MWE-based: P=3/5=0.6000 R=3/38=0.0789 F=0.1395
* VID: Tok-based: P=6/10=0.6000 R=6/138=0.0435 F=0.0811

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=465/500=93% pred=222/250=89%
* Continuous: MWE-based: P=204/222=0.9189 R=204/465=0.4387 F=0.5939
* Discontinuous: MWE-proportion: gold=35/500=7% pred=28/250=11%
* Discontinuous: MWE-based: P=9/28=0.3214 R=9/35=0.2571 F=0.2857

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=250/250=100%
* Multi-token: MWE-based: P=213/250=0.8520 R=213/500=0.4260 F=0.5680

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=286/500=57% pred=250/250=100%
* Seen-in-train: MWE-based: P=213/250=0.8520 R=213/286=0.7448 F=0.7948
* Unseen-in-train: MWE-proportion: gold=214/500=43% pred=0/250=0%
* Unseen-in-train: MWE-based: P=0/0=0.0000 R=0/214=0.0000 F=0.0000

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=140/286=49% pred=123/250=49%
* Variant-of-train: MWE-based: P=95/123=0.7724 R=95/140=0.6786 F=0.7224
* Identical-to-train: MWE-proportion: gold=146/286=51% pred=127/250=51%
* Identical-to-train: MWE-based: P=118/127=0.9291 R=118/146=0.8082 F=0.8645

