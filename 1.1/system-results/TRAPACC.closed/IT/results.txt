## Global evaluation
* MWE-based: P=151/288=0.5243 R=151/496=0.3044 F=0.3852
* Tok-based: P=392/637=0.6154 R=392/1292=0.3034 F=0.4064

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=19/288=7%
* IAV: MWE-based: P=13/19=0.6842 R=13/41=0.3171 F=0.4333
* IAV: Tok-based: P=30/42=0.7143 R=30/101=0.2970 F=0.4196
* IRV: MWE-proportion: gold=96/496=19% pred=125/288=43%
* IRV: MWE-based: P=54/125=0.4320 R=54/96=0.5625 F=0.4887
* IRV: Tok-based: P=110/251=0.4382 R=110/192=0.5729 F=0.4966
* LS.ICV: MWE-proportion: gold=8/496=2% pred=0/288=0%
* LS.ICV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* LS.ICV: Tok-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=25/496=5% pred=12/288=4%
* LVC.cause: MWE-based: P=4/12=0.3333 R=4/25=0.1600 F=0.2162
* LVC.cause: Tok-based: P=9/20=0.4500 R=9/53=0.1698 F=0.2466
* LVC.full: MWE-proportion: gold=104/496=21% pred=41/288=14%
* LVC.full: MWE-based: P=22/41=0.5366 R=22/104=0.2115 F=0.3034
* LVC.full: Tok-based: P=47/87=0.5402 R=47/231=0.2035 F=0.2956
* MVC: MWE-proportion: gold=5/496=1% pred=1/288=0%
* MVC: MWE-based: P=0/1=0.0000 R=0/5=0.0000 F=0.0000
* MVC: Tok-based: P=0/2=0.0000 R=0/11=0.0000 F=0.0000
* VID: MWE-proportion: gold=201/496=41% pred=87/288=30%
* VID: MWE-based: P=31/87=0.3563 R=31/201=0.1542 F=0.2153
* VID: Tok-based: P=93/229=0.4061 R=93/652=0.1426 F=0.2111
* VPC.full: MWE-proportion: gold=23/496=5% pred=3/288=1%
* VPC.full: MWE-based: P=2/3=0.6667 R=2/23=0.0870 F=0.1538
* VPC.full: Tok-based: P=4/6=0.6667 R=4/50=0.0800 F=0.1429

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=213/288=74%
* Continuous: MWE-based: P=125/213=0.5869 R=125/331=0.3776 F=0.4596
* Discontinuous: MWE-proportion: gold=165/496=33% pred=75/288=26%
* Discontinuous: MWE-based: P=26/75=0.3467 R=26/165=0.1576 F=0.2167

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=284/288=99%
* Multi-token: MWE-based: P=151/284=0.5317 R=151/496=0.3044 F=0.3872

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=158/288=55%
* Seen-in-train: MWE-based: P=129/158=0.8165 R=129/297=0.4343 F=0.5670
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=130/288=45%
* Unseen-in-train: MWE-based: P=22/130=0.1692 R=22/199=0.1106 F=0.1337

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=74/158=47%
* Variant-of-train: MWE-based: P=59/74=0.7973 R=59/184=0.3207 F=0.4574
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=84/158=53%
* Identical-to-train: MWE-based: P=70/84=0.8333 R=70/113=0.6195 F=0.7107

