## Global evaluation
* MWE-based: P=144/375=0.3840 R=144/501=0.2874 F=0.3288
* Tok-based: P=315/746=0.4223 R=315/1087=0.2898 F=0.3437

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=44/501=9% pred=16/375=4%
* IAV: MWE-based: P=8/16=0.5000 R=8/44=0.1818 F=0.2667
* IAV: Tok-based: P=16/32=0.5000 R=16/98=0.1633 F=0.2462
* LVC.cause: MWE-proportion: gold=36/501=7% pred=21/375=6%
* LVC.cause: MWE-based: P=0/21=0.0000 R=0/36=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/33=0.0000 R=0/72=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=166/501=33% pred=74/375=20%
* LVC.full: MWE-based: P=24/74=0.3243 R=24/166=0.1446 F=0.2000
* LVC.full: Tok-based: P=51/150=0.3400 R=51/340=0.1500 F=0.2082
* MVC: MWE-proportion: gold=4/501=1% pred=0/375=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/4=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* VID: MWE-proportion: gold=79/501=16% pred=31/375=8%
* VID: MWE-based: P=2/31=0.0645 R=2/79=0.0253 F=0.0364
* VID: Tok-based: P=7/65=0.1077 R=7/225=0.0311 F=0.0483
* VPC.full: MWE-proportion: gold=146/501=29% pred=232/375=62%
* VPC.full: MWE-based: P=80/232=0.3448 R=80/146=0.5479 F=0.4233
* VPC.full: Tok-based: P=160/464=0.3448 R=160/292=0.5479 F=0.4233
* VPC.semi: MWE-proportion: gold=26/501=5% pred=1/375=0%
* VPC.semi: MWE-based: P=0/1=0.0000 R=0/26=0.0000 F=0.0000
* VPC.semi: Tok-based: P=0/2=0.0000 R=0/52=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=295/501=59% pred=279/375=74%
* Continuous: MWE-based: P=123/279=0.4409 R=123/295=0.4169 F=0.4286
* Discontinuous: MWE-proportion: gold=206/501=41% pred=96/375=26%
* Discontinuous: MWE-based: P=21/96=0.2188 R=21/206=0.1019 F=0.1391

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=498/501=99% pred=366/375=98%
* Multi-token: MWE-based: P=144/366=0.3934 R=144/498=0.2892 F=0.3333
* Single-token: MWE-proportion: gold=3/501=1% pred=9/375=2%
* Single-token: MWE-based: P=0/9=0.0000 R=0/3=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=143/501=29% pred=165/375=44%
* Seen-in-train: MWE-based: P=94/165=0.5697 R=94/143=0.6573 F=0.6104
* Unseen-in-train: MWE-proportion: gold=358/501=71% pred=210/375=56%
* Unseen-in-train: MWE-based: P=50/210=0.2381 R=50/358=0.1397 F=0.1761

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=76/143=53% pred=87/165=53%
* Variant-of-train: MWE-based: P=43/87=0.4943 R=43/76=0.5658 F=0.5276
* Identical-to-train: MWE-proportion: gold=67/143=47% pred=78/165=47%
* Identical-to-train: MWE-based: P=51/78=0.6538 R=51/67=0.7612 F=0.7034

