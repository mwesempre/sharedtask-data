## Global evaluation
* MWE-based: P=206/802=0.2569 R=206/500=0.4120 F=0.3164
* Tok-based: P=538/1679=0.3204 R=538/1150=0.4678 F=0.3803

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=149/802=19%
* IAV: MWE-based: P=32/149=0.2148 R=32/64=0.5000 F=0.3005
* IAV: Tok-based: P=77/325=0.2369 R=77/148=0.5203 F=0.3256
* IRV: MWE-proportion: gold=121/500=24% pred=248/802=31%
* IRV: MWE-based: P=81/248=0.3266 R=81/121=0.6694 F=0.4390
* IRV: Tok-based: P=163/496=0.3286 R=163/242=0.6736 F=0.4417
* LVC.cause: MWE-proportion: gold=28/500=6% pred=70/802=9%
* LVC.cause: MWE-based: P=0/70=0.0000 R=0/28=0.0000 F=0.0000
* LVC.cause: Tok-based: P=2/81=0.0247 R=2/63=0.0317 F=0.0278
* LVC.full: MWE-proportion: gold=85/500=17% pred=33/802=4%
* LVC.full: MWE-based: P=11/33=0.3333 R=11/85=0.1294 F=0.1864
* LVC.full: Tok-based: P=23/70=0.3286 R=23/176=0.1307 F=0.1870
* MVC: MWE-proportion: gold=106/500=21% pred=236/802=29%
* MVC: MWE-based: P=49/236=0.2076 R=49/106=0.4623 F=0.2865
* MVC: Tok-based: P=126/534=0.2360 R=126/249=0.5060 F=0.3218
* VID: MWE-proportion: gold=95/500=19% pred=66/802=8%
* VID: MWE-based: P=10/66=0.1515 R=10/95=0.1053 F=0.1242
* VID: Tok-based: P=44/173=0.2543 R=44/270=0.1630 F=0.1986
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/802=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=694/802=87%
* Continuous: MWE-based: P=189/694=0.2723 R=189/359=0.5265 F=0.3590
* Discontinuous: MWE-proportion: gold=141/500=28% pred=108/802=13%
* Discontinuous: MWE-based: P=17/108=0.1574 R=17/141=0.1206 F=0.1365

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=741/802=92%
* Multi-token: MWE-based: P=206/741=0.2780 R=206/500=0.4120 F=0.3320

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=377/802=47%
* Seen-in-train: MWE-based: P=166/377=0.4403 R=166/279=0.5950 F=0.5061
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=425/802=53%
* Unseen-in-train: MWE-based: P=40/425=0.0941 R=40/221=0.1810 F=0.1238

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=123/377=33%
* Variant-of-train: MWE-based: P=54/123=0.4390 R=54/145=0.3724 F=0.4030
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=254/377=67%
* Identical-to-train: MWE-based: P=112/254=0.4409 R=112/134=0.8358 F=0.5773

