## Global evaluation
* MWE-based: P=8/489=0.0164 R=8/506=0.0158 F=0.0161
* Tok-based: P=44/837=0.0526 R=44/1045=0.0421 F=0.0468

## Per-category evaluation (partition of Global)
* LVC.cause: MWE-proportion: gold=0/506=0% pred=322/489=66%
* LVC.cause: MWE-based: P=0/322=0.0000 R=0/0=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/325=0.0000 R=0/0=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=272/506=54% pred=13/489=3%
* LVC.full: MWE-based: P=3/13=0.2308 R=3/272=0.0110 F=0.0211
* LVC.full: Tok-based: P=11/26=0.4231 R=11/548=0.0201 F=0.0383
* MVC: MWE-proportion: gold=1/506=0% pred=0/489=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* VID: MWE-proportion: gold=233/506=46% pred=7/489=1%
* VID: MWE-based: P=3/7=0.4286 R=3/233=0.0129 F=0.0250
* VID: Tok-based: P=8/14=0.5714 R=8/495=0.0162 F=0.0314
* VPC.full: MWE-proportion: gold=0/506=0% pred=147/489=30%
* VPC.full: MWE-based: P=0/147=0.0000 R=0/0=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/472=0.0000 R=0/0=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=208/506=41% pred=439/489=90%
* Continuous: MWE-based: P=3/439=0.0068 R=3/208=0.0144 F=0.0093
* Discontinuous: MWE-proportion: gold=298/506=59% pred=50/489=10%
* Discontinuous: MWE-based: P=5/50=0.1000 R=5/298=0.0168 F=0.0287

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=502/506=99% pred=170/489=35%
* Multi-token: MWE-based: P=8/170=0.0471 R=8/502=0.0159 F=0.0238
* Single-token: MWE-proportion: gold=4/506=1% pred=319/489=65%
* Single-token: MWE-based: P=0/319=0.0000 R=0/4=0.0000 F=0.0000

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=126/506=25% pred=10/489=2%
* Seen-in-train: MWE-based: P=3/10=0.3000 R=3/126=0.0238 F=0.0441
* Unseen-in-train: MWE-proportion: gold=380/506=75% pred=479/489=98%
* Unseen-in-train: MWE-based: P=5/479=0.0104 R=5/380=0.0132 F=0.0116

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=75/126=60% pred=7/10=70%
* Variant-of-train: MWE-based: P=0/7=0.0000 R=0/75=0.0000 F=0.0000
* Identical-to-train: MWE-proportion: gold=51/126=40% pred=3/10=30%
* Identical-to-train: MWE-based: P=3/3=1.0000 R=3/51=0.0588 F=0.1111

