## Global evaluation
* MWE-based: P=65/673=0.0966 R=65/500=0.1300 F=0.1108
* Tok-based: P=332/1043=0.3183 R=332/1150=0.2887 F=0.3028

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=0/673=0%
* IAV: MWE-based: P=0/0=0.0000 R=0/64=0.0000 F=0.0000
* IAV: Tok-based: P=0/0=0.0000 R=0/148=0.0000 F=0.0000
* IRV: MWE-proportion: gold=121/500=24% pred=0/673=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/121=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/242=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=28/500=6% pred=0/673=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/63=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=85/500=17% pred=0/673=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/85=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/176=0.0000 F=0.0000
* MVC: MWE-proportion: gold=106/500=21% pred=0/673=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/106=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/249=0.0000 F=0.0000
* <unlabeled>: MWE-proportion: gold=0/500=0% pred=673/673=100%
* <unlabeled>: MWE-based: P=0/673=0.0000 R=0/0=0.0000 F=0.0000
* <unlabeled>: Tok-based: P=0/1043=0.0000 R=0/0=0.0000 F=0.0000
* VID: MWE-proportion: gold=95/500=19% pred=0/673=0%
* VID: MWE-based: P=0/0=0.0000 R=0/95=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/270=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/673=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=634/673=94%
* Continuous: MWE-based: P=61/634=0.0962 R=61/359=0.1699 F=0.1229
* Discontinuous: MWE-proportion: gold=141/500=28% pred=39/673=6%
* Discontinuous: MWE-based: P=4/39=0.1026 R=4/141=0.0284 F=0.0444

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=318/673=47%
* Multi-token: MWE-based: P=65/318=0.2044 R=65/500=0.1300 F=0.1589

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=125/673=19%
* Seen-in-train: MWE-based: P=46/125=0.3680 R=46/279=0.1649 F=0.2277
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=548/673=81%
* Unseen-in-train: MWE-based: P=19/548=0.0347 R=19/221=0.0860 F=0.0494

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=60/125=48%
* Variant-of-train: MWE-based: P=15/60=0.2500 R=15/145=0.1034 F=0.1463
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=65/125=52%
* Identical-to-train: MWE-based: P=31/65=0.4769 R=31/134=0.2313 F=0.3116

