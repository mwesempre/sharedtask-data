mumpitz
Rafael Ehren, Timm Lichte and Younes Samih
(Submission #17)

Summary
A bidirectional recurrent neural network composed of LSTM units that uses the lemmas, POS tags and dependency relations as features. It only conducts binary classification, i.e. it decides if a token is part of a verbal multiword expression (VMWE) or not. So after the classification a heuristic is used that leverages the HEAD information provided in the original data to differentiate the VMWEs in a sentence.

Additional Fields
 	
Track type:  	
Description	Value(s)	Optional value
Please choose a TRACK TYPE (*)	CLOSED
 	
Languages:  	
Bulgarian (BG)
German (DE)
Greek (EL)
French (FR)
Polish (PL)
Brazilian Portuguese (PT)
Spanish (ES)
