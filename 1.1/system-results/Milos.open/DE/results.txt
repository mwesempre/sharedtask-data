## Global evaluation
* MWE-based: P=174/273=0.6374 R=174/498=0.3494 F=0.4514
* Tok-based: P=239/353=0.6771 R=239/975=0.2451 F=0.3599

## Per-category evaluation (partition of Global)
* IRV: MWE-proportion: gold=40/498=8% pred=0/273=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/40=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/96=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=2/498=0% pred=0/273=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=42/498=8% pred=0/273=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/42=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/97=0.0000 F=0.0000
* VID: MWE-proportion: gold=183/498=37% pred=0/273=0%
* VID: MWE-based: P=0/0=0.0000 R=0/183=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/462=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=210/498=42% pred=273/273=100%
* VPC.full: MWE-based: P=150/273=0.5495 R=150/210=0.7143 F=0.6211
* VPC.full: Tok-based: P=198/353=0.5609 R=198/291=0.6804 F=0.6149
* VPC.semi: MWE-proportion: gold=23/498=5% pred=0/273=0%
* VPC.semi: MWE-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000
* VPC.semi: Tok-based: P=0/0=0.0000 R=0/31=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=267/498=54% pred=199/273=73%
* Continuous: MWE-based: P=122/199=0.6131 R=122/267=0.4569 F=0.5236
* Discontinuous: MWE-proportion: gold=231/498=46% pred=74/273=27%
* Discontinuous: MWE-based: P=52/74=0.7027 R=52/231=0.2251 F=0.3410

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=349/498=70% pred=80/273=29%
* Multi-token: MWE-based: P=56/80=0.7000 R=56/349=0.1605 F=0.2611
* Single-token: MWE-proportion: gold=149/498=30% pred=193/273=71%
* Single-token: MWE-based: P=118/193=0.6114 R=118/149=0.7919 F=0.6901

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=250/498=50% pred=129/273=47%
* Seen-in-train: MWE-based: P=110/129=0.8527 R=110/250=0.4400 F=0.5805
* Unseen-in-train: MWE-proportion: gold=248/498=50% pred=144/273=53%
* Unseen-in-train: MWE-based: P=64/144=0.4444 R=64/248=0.2581 F=0.3265

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=148/250=59% pred=69/129=53%
* Variant-of-train: MWE-based: P=60/69=0.8696 R=60/148=0.4054 F=0.5530
* Identical-to-train: MWE-proportion: gold=102/250=41% pred=60/129=47%
* Identical-to-train: MWE-based: P=50/60=0.8333 R=50/102=0.4902 F=0.6173

