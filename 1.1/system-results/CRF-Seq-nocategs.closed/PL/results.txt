## Global evaluation
* MWE-based: P=241/340=0.7088 R=241/515=0.4680 F=0.5637
* Tok-based: P=563/676=0.8328 R=563/1108=0.5081 F=0.6312

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=29/515=6% pred=340/340=100%
* IAV: MWE-based: P=14/340=0.0412 R=14/29=0.4828 F=0.0759
* IAV: Tok-based: P=42/676=0.0621 R=42/64=0.6562 F=0.1135
* IRV: MWE-proportion: gold=249/515=48% pred=0/340=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/249=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/499=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=15/515=3% pred=0/340=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/15=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/33=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=149/515=29% pred=0/340=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/149=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/305=0.0000 F=0.0000
* VID: MWE-proportion: gold=73/515=14% pred=0/340=0%
* VID: MWE-based: P=0/0=0.0000 R=0/73=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/207=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=361/515=70% pred=315/340=93%
* Continuous: MWE-based: P=223/315=0.7079 R=223/361=0.6177 F=0.6598
* Discontinuous: MWE-proportion: gold=154/515=30% pred=25/340=7%
* Discontinuous: MWE-based: P=18/25=0.7200 R=18/154=0.1169 F=0.2011

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=515/515=100% pred=300/340=88%
* Multi-token: MWE-based: P=241/300=0.8033 R=241/515=0.4680 F=0.5914

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=371/515=72% pred=242/340=71%
* Seen-in-train: MWE-based: P=226/242=0.9339 R=226/371=0.6092 F=0.7374
* Unseen-in-train: MWE-proportion: gold=144/515=28% pred=98/340=29%
* Unseen-in-train: MWE-based: P=15/98=0.1531 R=15/144=0.1042 F=0.1240

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=223/371=60% pred=106/242=44%
* Variant-of-train: MWE-based: P=95/106=0.8962 R=95/223=0.4260 F=0.5775
* Identical-to-train: MWE-proportion: gold=148/371=40% pred=136/242=56%
* Identical-to-train: MWE-based: P=131/136=0.9632 R=131/148=0.8851 F=0.9225

