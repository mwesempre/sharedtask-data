## Global evaluation
* MWE-based: P=134/243=0.5514 R=134/496=0.2702 F=0.3627
* Tok-based: P=427/544=0.7849 R=427/1292=0.3305 F=0.4651

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=41/496=8% pred=243/243=100%
* IAV: MWE-based: P=10/243=0.0412 R=10/41=0.2439 F=0.0704
* IAV: Tok-based: P=43/544=0.0790 R=43/101=0.4257 F=0.1333
* IRV: MWE-proportion: gold=96/496=19% pred=0/243=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/96=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/192=0.0000 F=0.0000
* LS.ICV: MWE-proportion: gold=8/496=2% pred=0/243=0%
* LS.ICV: MWE-based: P=0/0=0.0000 R=0/8=0.0000 F=0.0000
* LS.ICV: Tok-based: P=0/0=0.0000 R=0/17=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=25/496=5% pred=0/243=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/25=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/53=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=104/496=21% pred=0/243=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/104=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/231=0.0000 F=0.0000
* MVC: MWE-proportion: gold=5/496=1% pred=0/243=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/5=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/11=0.0000 F=0.0000
* VID: MWE-proportion: gold=201/496=41% pred=0/243=0%
* VID: MWE-based: P=0/0=0.0000 R=0/201=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/652=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=23/496=5% pred=0/243=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/23=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/50=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=331/496=67% pred=237/243=98%
* Continuous: MWE-based: P=132/237=0.5570 R=132/331=0.3988 F=0.4648
* Discontinuous: MWE-proportion: gold=165/496=33% pred=6/243=2%
* Discontinuous: MWE-based: P=2/6=0.3333 R=2/165=0.0121 F=0.0234

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=496/496=100% pred=202/243=83%
* Multi-token: MWE-based: P=134/202=0.6634 R=134/496=0.2702 F=0.3840

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=297/496=60% pred=136/243=56%
* Seen-in-train: MWE-based: P=118/136=0.8676 R=118/297=0.3973 F=0.5450
* Unseen-in-train: MWE-proportion: gold=199/496=40% pred=107/243=44%
* Unseen-in-train: MWE-based: P=16/107=0.1495 R=16/199=0.0804 F=0.1046

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=184/297=62% pred=57/136=42%
* Variant-of-train: MWE-based: P=51/57=0.8947 R=51/184=0.2772 F=0.4232
* Identical-to-train: MWE-proportion: gold=113/297=38% pred=79/136=58%
* Identical-to-train: MWE-based: P=67/79=0.8481 R=67/113=0.5929 F=0.6979

