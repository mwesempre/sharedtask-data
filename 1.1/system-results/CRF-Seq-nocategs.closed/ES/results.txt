## Global evaluation
* MWE-based: P=180/583=0.3087 R=180/500=0.3600 F=0.3324
* Tok-based: P=480/1281=0.3747 R=480/1150=0.4174 F=0.3949

## Per-category evaluation (partition of Global)
* IAV: MWE-proportion: gold=64/500=13% pred=583/583=100%
* IAV: MWE-based: P=35/583=0.0600 R=35/64=0.5469 F=0.1082
* IAV: Tok-based: P=95/1281=0.0742 R=95/148=0.6419 F=0.1330
* IRV: MWE-proportion: gold=121/500=24% pred=0/583=0%
* IRV: MWE-based: P=0/0=0.0000 R=0/121=0.0000 F=0.0000
* IRV: Tok-based: P=0/0=0.0000 R=0/242=0.0000 F=0.0000
* LVC.cause: MWE-proportion: gold=28/500=6% pred=0/583=0%
* LVC.cause: MWE-based: P=0/0=0.0000 R=0/28=0.0000 F=0.0000
* LVC.cause: Tok-based: P=0/0=0.0000 R=0/63=0.0000 F=0.0000
* LVC.full: MWE-proportion: gold=85/500=17% pred=0/583=0%
* LVC.full: MWE-based: P=0/0=0.0000 R=0/85=0.0000 F=0.0000
* LVC.full: Tok-based: P=0/0=0.0000 R=0/176=0.0000 F=0.0000
* MVC: MWE-proportion: gold=106/500=21% pred=0/583=0%
* MVC: MWE-based: P=0/0=0.0000 R=0/106=0.0000 F=0.0000
* MVC: Tok-based: P=0/0=0.0000 R=0/249=0.0000 F=0.0000
* VID: MWE-proportion: gold=95/500=19% pred=0/583=0%
* VID: MWE-based: P=0/0=0.0000 R=0/95=0.0000 F=0.0000
* VID: Tok-based: P=0/0=0.0000 R=0/270=0.0000 F=0.0000
* VPC.full: MWE-proportion: gold=1/500=0% pred=0/583=0%
* VPC.full: MWE-based: P=0/0=0.0000 R=0/1=0.0000 F=0.0000
* VPC.full: Tok-based: P=0/0=0.0000 R=0/2=0.0000 F=0.0000

## MWE continuity (partition of Global)
* Continuous: MWE-proportion: gold=359/500=72% pred=568/583=97%
* Continuous: MWE-based: P=175/568=0.3081 R=175/359=0.4875 F=0.3776
* Discontinuous: MWE-proportion: gold=141/500=28% pred=15/583=3%
* Discontinuous: MWE-based: P=5/15=0.3333 R=5/141=0.0355 F=0.0641

## Number of tokens (partition of Global)
* Multi-token: MWE-proportion: gold=500/500=100% pred=547/583=94%
* Multi-token: MWE-based: P=180/547=0.3291 R=180/500=0.3600 F=0.3438

## Whether seen in train (partition of Global)
* Seen-in-train: MWE-proportion: gold=279/500=56% pred=322/583=55%
* Seen-in-train: MWE-based: P=144/322=0.4472 R=144/279=0.5161 F=0.4792
* Unseen-in-train: MWE-proportion: gold=221/500=44% pred=261/583=45%
* Unseen-in-train: MWE-based: P=36/261=0.1379 R=36/221=0.1629 F=0.1494

## Whether identical to train (partition of Seen-in-train)
* Variant-of-train: MWE-proportion: gold=145/279=52% pred=108/322=34%
* Variant-of-train: MWE-based: P=44/108=0.4074 R=44/145=0.3034 F=0.3478
* Identical-to-train: MWE-proportion: gold=134/279=48% pred=214/322=66%
* Identical-to-train: MWE-based: P=100/214=0.4673 R=100/134=0.7463 F=0.5747

