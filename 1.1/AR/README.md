README
======
This is a placeholder README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Arabic, edition 1.1.

The Arabic corpus does not have an open licence.
Participants are required to fill in an agreement and obtain the corpus through LDC.

Therefore, please follow these steps:
  * Download the [licence agreement](https://gitlab.com/parseme/sharedtask-data/raw/master/1.1/AR/2018_PARSEME_Shared_Task_LDC_License_Agreement.pdf)
  * Read it and, if you agree, fill it in and sign it
  * Send the agreement to [LDC](mailto:ldc@ldc.upenn.edu) as indicated on the document
  * The training data will be made available by LDC once the agreement is signed and they have processes the request

The test data will be made available later by LDC, but you only need to sign and send the agreement once.

Given that this is a late addition, Arabic will be considered as optional this year.
This means that we will publish generic and per-category rankings for teams who address Arabic, but it will not be included in the macro-average rankings across languages.
